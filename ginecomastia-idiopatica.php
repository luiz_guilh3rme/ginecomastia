<?php 
$bodyClass = 'interna';
$title = 'Ginecomastia Idiopática | Ginecomastia Tratamento';
$description = 'Ginecomastia Idiopática é causada por um desequilibrio hormonal no homem, aumentando a produção de estrogêneo. Clique e Saiba mais! ';
$cannonical = 'https://www.ginecomastiatratamento.com.br/ginecomastia-idiopatica/';
$message = 'Entre em contato conosco';
$type = 'contato';
include 'header.php';

?>
<div itemscope itemtype="http://schema.org/WebPage">
	<div class="container">
		<div class="row">
		<div class="breadcrumb">
				<ul itemscope itemtype="http://schema.org/BreadcrumbList">
					<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
						<a itemprop="item" href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/"><i class="fa fa-home" ></i>
						<span itemprop="name">home</span>
						</a>
						<meta itemprop="position" content="1" />
					</li>
					<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
						<a itemprop="item" href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/causas-da-ginecomastia/">
						<span itemprop="name">Causas da Ginecomastia</span>
						</a>
						<meta itemprop="position" content="2" />
					</li>
					<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
						<span itemprop="name" class="active">Ginecomastia Idiopática</span>
						<meta itemprop="position" content="3" />
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<section class="main-content">
	<div class="container">
		<div class="row row-border">
			<div class="content">
				<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">
					<h1 class="text-uppercase section-title text-blue">Ginecomastia Idiopática</h1>
					<p>
						Mais de 60% de todos os casos de Ginecomastia tem causa idiopática. Derivada do grego idios (pessoal, próprio) + patheia (sofrimento), uma causa idiopática significa ter origem desconhecida ou de surgimento espontâneo.
					</p>
					<p>
						Em seu livro "The Human Body" (O corpo humano), Isaac Asimov escreveu um comentário feito sobre o termo "Idiopático" na 20ª edição do Stedman's Medical Dictionary: "um termo pretensioso para ocultar ignorância". Muito provavelmente nas próximas décadas, com o avanço do estudo do genoma humano, muitas das causas de ginecomastia que hoje ainda são desconhecidas, terão suas causas descobertas.
					</p>
					
				</div>
				<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6"> 
					<div class="formulario">
						<?php include 'form-topo.php';?>
					</div>		
				</div>
			</div>
		</div>
		<div class="padding"></div>
		<div class="row">
			<div class="col-lg-9 row-border">
				<div class="col-sm-12 col-xs-12 col-lg-5 col-md-5">
					<div class="row">
						<div class="content">
							<p>
								Ela ocorre por conta de um problema de balanço hormonal no homem. Os testículos produzem os principais hormônios masculinos, que são denominados andrógenos (compostos por testosterona, diidrotestosterona e androstenediona) e são responsáveis pelas características masculinas. Já os hormônios que promovem os caracteres femininos são chamados de estrógenos, sendo os principais o estrógeno e a progesterona. 
							</p>
							<p>
								Os testículos também produzem estrógenos no sexo masculino, mas em pequena quantidade. Assim, a principal fonte de estrógeneos no homem provém da conversão da testosterona e do androstenediol em outros tecidos do organismo, sobretudo no fígado, através de uma enzima chamada aromatase, constituindo provavelmente, até 80% da produção masculina total de estrógeneos.
							</p>
							<p>
								O tecido mamário normal contém receptores para estrógenos e andrógenos. Os estrógenos estimulam a proliferação dos ductos mamários e consequentemente o desenvolvimento das mamas, já os andrógenos inibem este processo. Uma relação aumentada nos níveis de estrógenos em relação aos andrógenos no tecido mamário parece ser responsável pela a maioria dos casos de ginecomastia. 
							</p>
						</div>
						
					</div>
				</div>
				<div class="col-sm-12 col-xs-12 col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1">
					<div class="row">
						<div class="content">
							<h3 class="article-subtitle text-uppercase">Quais são as causas?</h3>
							<p>
								A produção excessiva de estrógenos no homem parece ser uma das principais causas da ginecomastia idiopática, mas outros fatores também importantes são a degradação diminuída dos hormônios femininos pelo organismo ou alterações genéticas nos receptores de andrógenos e estrógenos no tecido mamário.
							</p>
							<p>
								Estudos científicos recentes descobriram que os receptores das mamas destes pacientes podem ser mais sensíveis à ação de estrógenos que no resto da população e dessa maneira há um maior crescimento nas mamas desses pacientes.
							</p>
						</div>
						
					</div>		
				</div>
			</div>
			<div class="col-sm-12 col-xs-12 col-md-3 col-lg-3 submenu">
				<div class="text-uppercase indice-title"><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/causas-da-ginecomastia/">Causas da Ginecomastia:</a></div>
				<ul>
					<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-idiopatica/" class="active">Ginecomastia Idiopática</a></li>
					<li>
						<a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-fisiologica/">
							Ginecomastia Fisiológica
						</a>
						<ul class="sublist">
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-puberal/">Ginecomastia Puberal</a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-senil/">Ginecomastia Senil</a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-neonatal/">Ginecomastia Neonatal</a></li>
						</ul>
					</li>
					<li>
						<a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-patologica/">
							Ginecomastia Patológica
						</a>
						<ul class="sublist">
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-patologica-nao-tumorais/">Ginecomastia Não-Tumoral</a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-patologica-tumoral/">Ginecomastia Tumoral</a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-causas-medicamentosas/">Ginecomastia Medicamentosa</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>

<section class="mais">
	<div class="container">
		<div class="row">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 articles">
					<h2 class="section-title article-title">Conheça mais sobre Ginecomastia</h2>
					<div class="row">
						<?php
						include 'includes/partials/o-que-e.php';
						include 'includes/partials/graus.php';
						include 'includes/partials/causas.php';
						include 'includes/partials/tratamento.php';
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<?php 
include 'ask.php';
include 'footer.php';
?>