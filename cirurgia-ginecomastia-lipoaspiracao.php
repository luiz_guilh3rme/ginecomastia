<?php 
$bodyClass = 'interna';
$title = 'Cirurgia de Ginecomastia: Lipoaspiração | Ginecomastia Tratamento';
$description = 'Cirurgia de Ginecomastia: Lipoaspiração - O objetivo dessa técnica cirúrgica é aspirar a gordura localizada por meio de cânulas. Saiba mais!';
$cannonical = 'https://www.ginecomastiatratamento.com.br/cirurgia-ginecomastia-lipoaspiracao/';
$message = 'Entre em contato conosco';
$type = 'contato';
include 'header.php';

?>
<div itemscope itemtype="http://schema.org/WebPage">
	<div class="container">
		<div class="row">
		<div class="breadcrumb">
				<ul itemscope itemtype="http://schema.org/BreadcrumbList">
					<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
						<a itemprop="item" href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/"><i class="fa fa-home" ></i>
						<span itemprop="name">home</span>
						</a>
						<meta itemprop="position" content="1" />
					</li>
					<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
						<a itemprop="item" href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/tratamento-para-ginecomastia/">
						<span itemprop="name">Tratamentos da Ginecomastia</span>
						</a>
						<meta itemprop="position" content="2" />
					</li>
					<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
						<span itemprop="name" class="active">Cirurgia de Ginecomastia Lipoaspiração</span>
						<meta itemprop="position" content="3" />
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<section class="main-content">
	<div class="container">
		<div class="row row-border">
			<div class="content">
				<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">
					<h1 class="text-uppercase section-title text-blue">Cirurgia de Ginecomastia Lipoaspiração</h1>
					<p>
						A lipoaspiração foi criada em 1977 pelo ginecologista francês Yves Gerard Illouz que inventou o procedimento para resolver o problema da namorada - uma atriz famosa cujo nome ele não revela -, que não podia usar decotes nas costas por causa de um lipoma (tumor benigno formado por células gordurosas). Ela consiste na aspiração do tecido adiposo (gordura) com uma cânula fina e multi-perfurada, conectada a um aspirador à vácuo.
					</p>

				</div>
				<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6"> 
					<div class="formulario">
						<?php include 'form-topo.php';?>
					</div>		
				</div>
			</div>
		</div>
		<div class="padding"></div>
		<div class="row">
			<div class="col-lg-9 row-border">
				<div class="col-sm-12 col-xs-12 col-lg-5 col-md-5">
					<div class="row">
						<div class="content">
							<h2 class="article-subtitle">Procedimentos da <span class="text-bold">Lipoaspiração</span></h2>
							<p>
								Primeiramente é infiltrada uma solução composta por soro fisiológico, anestésicos e adrenalina – para diminuir o sangramento. As cânulas são introduzidas através da pele por pequenos orifícios e chegam ao tecido adiposo (camada que vem logo após a pele), de onde aspiram a gordura localizada.
							</p>
							<h2 class="article-subtitle">Benefícios da <span class="text-bold">Lipoaspiração</span></h2>
							<p>
								Atualmente a lipoaspiração a laser tem se tornado mais popular. O laser promove a destruição das células de gordura e estudos demonstram haver maior retração de pele quando comparada a lipoaspiração convencional. 
							</p>

						</div>

					</div>
				</div>
				<div class="col-sm-12 col-xs-12 col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1">
					<div class="row">
						<div class="content">
							<h2 class="article-subtitle">Em quais casos aplica o método cirúrgico da <span class="text-bold">Lipoaspiração?</span></h2>
							<p>
								Quando o paciente apresenta lipomastia, ou seja, aumento do volume mamário de componente apenas adiposo, a lipoaspiração é a técnica de escolha. Entretanto, em todos os outros casos de ginecomastia verdadeira, ou seja, com presença de tecido glandular mamário, a lipoaspiração pode ser indicada como parte do procedimento, pois além de reduzir o volume da gordura das mamas, facilita a dissecção cirúrgica e promove um retalho de pele mais regular.
							</p>
							<h2 class="article-subtitle">Pós-operatório da <span class="text-bold">Lipoaspiração</span></h2>
							<p>
								No pós operatório, as regiões lipoaspiradas podem ficar doloridas e com equimose (áreas de coloração arroxeadas) que melhoram em cerca de 1 a 2 semanas, mesmo período que o paciente pode voltar a fazer atividades físicas.
							</p>
						</div>

					</div>		
				</div>
			</div>
			<div class="col-sm-12 col-xs-12 col-md-3 col-lg-3 submenu">
				<div class="text-uppercase indice-title"><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/tratamento-para-ginecomastia/">Tratamentos da Ginecomastia: </div>
				<ul>
					<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-tratamento-medicamentoso/">Tratamento Medicamentoso</a></li>
					<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/cirurgia-ginecomastia/">Cirugia Ginecomastia </a></li>
					<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-pre-operatorio/">Pré-Operatório</a></li>
					<li class="has-sublist">
						Procedimentos
						<ul class="sublist">
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/cirurgia-ginecomastia-lipoaspiracao/" class="active">Lipoaspiração</a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/cirurgia-ginecomastia-incisao-webster/">Incisão de webster</a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/cirurgia-ginecomastia-incisao-periareolar-circular/">Incisão Peri-Areolar Circular</a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/cirurgia-ginecomastia-incisao-periareolar-prolongamento-medial-lateral/">Incisão Peri-Areolar + Incisão no Tórax </a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/cirurgia-ginecomastia-incisao-periareolar-sulco-inframamario/">Incisão Peri-Areolar + Incisão submamária</a></li>
						</ul>
					</li>
					<li>
						<a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-pos-operatorio/">Pós-Operatório</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>

<section class="mais">
	<div class="container">
		<div class="row">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 articles">
					<h2 class="section-title article-title">Conheça mais sobre Ginecomastia</h2>
					<div class="row">
						<?php
						include 'includes/partials/o-que-e.php';
						include 'includes/partials/graus.php';
						include 'includes/partials/causas.php';
						include 'includes/partials/tratamento.php';
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php 
include 'ask.php';
include 'footer.php';
?>
