<?php 
$bodyClass = 'interna';
$title = 'Ginecomastia Causas Tumoral | Ginecomastia Tratamento';
$description = 'Ginecomastia Tumoral - O desenvolvimento de alguns tipos de tumores (como testiculares e germinativos) podem desencadear a ginecomastia. Saiba mais!';
$cannonical = 'https://www.ginecomastiatratamento.com.br/ginecomastia-patologica-tumoral/';
$message = 'Entre em contato conosco';
$type = 'contato';
include 'header.php';

?>
<div itemscope itemtype="http://schema.org/WebPage">
	<div class="container">
		<div class="row">
		<div class="breadcrumb">
				<ul itemscope itemtype="http://schema.org/BreadcrumbList">
					<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
						<a itemprop="item" href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/"><i class="fa fa-home" ></i>
						<span itemprop="name">home</span>
						</a>
						<meta itemprop="position" content="1" />
					</li>
					<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
						<a itemprop="item" href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/causas-da-ginecomastia/">
						<span itemprop="name">Causas da Ginecomastia</span>
						</a>
						<meta itemprop="position" content="2" />
					</li>
					<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
						<span itemprop="name" class="active">Ginecomastia Tumoral</span>
						<meta itemprop="position" content="3" />
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<section class="main-content">
	<div class="container">
		<div class="row row-border">
			<div class="content">
				<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">
					<h1 class="text-uppercase section-title text-blue">Ginecomastia patológica tumoral</h1>

				</div>
				<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6"> 
					<div class="formulario">
						<?php include 'form-topo.php';?>
					</div>		
				</div>
			</div>
		</div>
		<div class="padding"></div>
		<div class="row">
			<div class="col-lg-9 row-border">
				<div class="col-sm-12 col-xs-12 col-lg-5 col-md-5">
					<div class="row">
						<div class="content">
							<h3 class="article-subtitle">Tumores Testiculares</h3>
							<p>
								Os tumores dos testículos podem acarretar níveis sanguíneos elevados de estrógenos e desencader Ginecomastia. São eles: Tumores das células de Leydig, os tumores das células de Sertoli, tumores germinativos.
							</p>
							<h3 class="article-subtitle">Tumores das Células de Leydig</h3>
							<p>
								Eles ocorrem, frequentemente, em homens entre 20 e 60 anos, mas somente em 25% ocorrem na pré-puberdade. 
							</p>
							<p>
								Clinicamente existe precocidade sexual, crescimento rápido com idade óssea superior à idade real, níveis elevados de testosterona e de estrógenos, associados a uma massa testicular palpável e ginecomastia. 
							</p>
							<p>
								Apesar de na sua maioria ser benigno, os tumores das células de Leydig podem ser malignos e disseminar para o pulmão, fígado, e nódulos linfáticos retroperitoniais.
							</p>
							<h3 class="article-subtitle">Tumores das Células de Sertoli</h3>
							<p>
								Os tumores das células de Sertoli correspondem a menos de 1% dos tumores testiculares e ocorrem em qualquer idade, sendo que 1/3 atinge rapazes com menos de 13 anos, frequentemente bebês com menos de 6 meses. 
							</p>
							<p>
								Apesar de atingir crianças em idades muito jovens não provoca efeitos endócrinos. É um tumor benigno em 90% dos casos. A ginecomastia ocorrem em 26 a 33% dos casos. Após orquidectomia geralmente a ginecomastia regressa.
							</p>
							<h3 class="article-subtitle">Tumores Germinativos</h3>
							<p>
								Essa enfermidade representa o tumor mais comum nos homens entre 15 e 35 anos. Ele inclui o carcinoma embrionário, o coriocarcinoma, o seminoma e o teratoma. 
							</p>
						</div>
					</div>
				</div>
				<div class="col-sm-12 col-xs-12 col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1">
					<div class="row">
						<div class="content">
							
							<p>
								A ginecomastia é causada por um aumento da produção de estrógenos pelos testículos. Essa produção também pode ocorrer fora das gônadas e não é exclusiva dos tumores. 
							</p>
							<p>
								Estudos revelam que homens com tumores das células germinativas e ginecomastia têm uma mortalidade mais elevada do que aqueles sem ginecomastia.
							</p>
							<h3 class="article-subtitle">Tumores não-testiculares</h3>
							<p>
								Os tumores primários do córtex supra-renal são raros e estão associados a várias anomalias congênitas, incluindo o astrocitoma, lesões cutâneas, hemihipertrofia, anomalias da supra-renal contralateral bem como a síndrome de Beckwith-Wiedmann. 
							</p>
							<p>
								Deve-se suspeitar deste tipo de tumor na presença de uma criança com sinais prematuros de virilização ou feminização, especialmente se houver ginecomastia associada. 
							</p>
							<p>
								Outros problemas, como o câncer de pulmão pode secretar hCG com consequente estimulação dos testículos e glândulas supra-renais a produzirem estrógenos. 
							</p>
							<p>
								A carcinoma hepatocelular poderá originar ginecomastia. A feminização derivada do carcinoma hepático primário é consequente ao aumento da actividade da aromatase que aumenta a conversão dos andrógenos em estrógenos.
							</p>
							<p>
								Por fim, os prolactinomas representam 5 a 10% dos tumores hipofisários. A prolactina em si não produz ginecomastia mas pode induzi-la devido ao hipogonadismo secundário que origina. Qualquer tumor com efeito de massa da área hipotalamo-hipófise pode provocar um hipogonadismo secundário, que na sua evolução poderá cursar com ginecomastia.
							</p>
						</div>
						
					</div>		
				</div>
			</div>
			<div class="col-sm-12 col-xs-12 col-md-3 col-lg-3 submenu">
				<div class="text-uppercase indice-title"><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/causas-da-ginecomastia/">Causas da Ginecomastia:</a></div>
				<ul>
					<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-idiopatica/">Ginecomastia Idiopática</a></li>
					<li>
						<a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-fisiologica/">
							Ginecomastia Fisiológica
						</a>
						<ul class="sublist">
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-puberal/">Ginecomastia Puberal</a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-senil/">Ginecomastia Senil</a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-neonatal/">Ginecomastia Neonatal</a></li>
						</ul>
					</li>
					<li>
						<a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-patologica/">
							Ginecomastia Patológica
						</a>
						<ul class="sublist">
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-patologica-nao-tumorais/">Ginecomastia Não-Tumoral</a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-patologica-tumoral/" class="active">Ginecomastia Tumoral</a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-causas-medicamentosas/">Ginecomastia Medicamentosa</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>
<section class="mais">
	<div class="container">
		<div class="row">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 articles">
					<h2 class="section-title article-title">Conheça mais sobre Ginecomastia</h2>
					<div class="row">
						<?php
						include 'includes/partials/o-que-e.php';
						include 'includes/partials/graus.php';
						include 'includes/partials/causas.php';
						include 'includes/partials/tratamento.php';
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<?php 
include 'ask.php';
include 'footer.php';
?>
