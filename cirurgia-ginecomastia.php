<?php 
$bodyClass = 'interna';
$title = 'Cirurgia Ginecomastia | Ginecomastia Tratamento';
$description = 'Cirurgia Ginecomastia - Existem vários métodos de cirurgias que podem ser realizadas, trazendo resultados muito satisfatórios. Confira quais são elas!';
$cannonical = 'https://www.ginecomastiatratamento.com.br/cirurgia-ginecomastia/';
$message = 'Entre em contato conosco';
$type = 'contato';
include 'header.php';

?>
<div itemscope itemtype="http://schema.org/WebPage">
	<div class="container">
		<div class="row">
			<div class="breadcrumb">
				<ul itemscope itemtype="http://schema.org/BreadcrumbList">
					<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
						<a itemprop="item" href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/"><i class="fa fa-home" ></i>
							<span itemprop="name">home</span>
						</a>
						<meta itemprop="position" content="1" />
					</li>
					<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
						<a itemprop="item" href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/tratamento-para-ginecomastia/">
							<span itemprop="name">Tratamentos da Ginecomastia</span>
						</a>
						<meta itemprop="position" content="2" />
					</li>
					<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
						<span itemprop="name" class="active">Cirurgia Ginecomastia</span>
						<meta itemprop="position" content="3" />
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<section class="main-content">
	<div class="container">
		<div class="row row-border">
			<div class="content">
				<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">
					<h1 class="text-uppercase section-title text-blue">Ginecomastia: Tratamento Cirúrgico</h1>
					<p>
						O tratamento cirúrgico é instituído em casos de mais de 12 a 18 meses de progressão e que não tiveram regressão completa com o uso de medicações ou de forma espontânea. Em pacientes com graus avançados que cursem com grandes impactos psicológicos, a cirurgia pode ser antecipada. 
					</p>
					<p>
						Além disso, o tratamento cirúrgico só deve ser indicado após a resolução de causas subjacentes (como perda de peso, tratamento de tumores, suspensão do consumo de substâncias como hormônios, anabolizantes, medicamentos, álcool ou maconha).			
					</p>
					
					<h2 class="article-subtitle">Ginecomastia: qual o objetivo do <span class="text-bold">tratamento cirúrgico?</span></h2>
					<p>
						O objetivo do tratamento cirúrgico é a ressecção do tecido mamário anômalo e a restauração do contorno do tórax masculino, com cicatriz mínima, sem deformidade residual e sem sofrimento do complexo aréolo-papilar.
					</p>

					<p>
						A cirurgia é realizada sob anestesia local + sedação ou anestesia geral e tem duração entre 1 a 3 horas dependendo da classificação da Ginecomastia.
					</p>

				</div>
				<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6"> 
					<div class="formulario">
						<?php include 'form-topo.php';?>
					</div>		
				</div>
			</div>
		</div>
		<div class="padding"></div>
		<div class="row">
			<div class="col-lg-9 row-border">
				<div class="col-sm-12 col-xs-12 col-lg-12 col-md-12">
					<div class="row">
					<div class="content">
							<h2 class="article-subtitle">Ginecomastia: <span class="text-bold">Modelos de Cirurgia</span></h2>

							<p>
								Quando o volume mamário presente é de natureza puramente adiposa (lipomastia ou pseudoginecomastia), idealmente é realizada uma lipoaspiração tradicional ou a laser.
							</p>

							<p>
								Nos casos de Ginecomastia "verdadeira", ou seja, com a presença de componente glandular, o tipo de tratamento cirúrgico dependerá do grau de hipertrofia da glândula mamária e da quantidade de sobra de pele presente.
							</p>

							<p>
								Em todos os casos, pode-se, ou não, iniciar a cirurgia com lipoaspiração para retirada do componente gorduroso e melhor isolamento da glândula. Veja os exemplos de acordo com cada grau de ginecomastia:
							</p>

							<ul>
								<li>Nas fases 1 e 2A em que não há sobra de pele, a retirada da glândula mamária pode ser realizada através de uma incisão periareolar inferior de Webster. <a href="https://www.ginecomastiatratamento.com.br/graus-da-ginecomastia/">Leia mais!</a></li><br>
								<li>Na fase 2B em que há moderada quantidade de glândula e sobra de pele, a cirurgia pode ser feita através de uma incisão periareolar circular completa, por onde se retira a glândula mamária e o excesso de pele. A cicatriz resultante fica em torno da aréola. <a href="https://www.ginecomastiatratamento.com.br/graus-da-ginecomastia/">Leia mais!</a></li><br>
								<li>Na fase 3 em que há grandes excessos de pele e de glândula mamária são necessárias maiores ressecções de pele com cicatrizes resultantes em torno da aréola + no sulco inframamário ou no tórax. <a href="https://www.ginecomastiatratamento.com.br/graus-da-ginecomastia/">Leia mais!</a></li>
							</ul>



						</div>
						
					</div>
				</div>

				<div class="col-sm-12 col-xs-12 col-lg-12 col-md-12">
					<div class="row">
						<div class="content">
							<h2 class="article-subtitle">Como escolher uma clínica especializada em <span class="text-bold">Ginecomastia?</span></h2>

							<ul>
								<li>Escolha uma clínica de Cirurgia Plástica conceituada;</li>
								<li>Marque uma consulta com um Cirurgião Plástico;</li>
								<li>Faça a intervenção cirúrgica somente num hospital equipado com UTI;</li>
								<li>Histórico de seus profissionais; </li>
								<li>Medicamentos que serão utilizados durante o processo.</li>

							</ul>

						</div>

							<div class="content">
								<h2 class="article-subtitle">Procure um <span class="text-bold">profissional adequado</span></h2>
							<p>
								Alguns dos médicos que poderão lhe auxiliar neste momento são os de formação em clínica geral, mastologia, pediatria, urologia, entre outros. No entanto, com o Dr. Wendell Uguetto, você vai receber todo o atendimento necessário, através de um exame completo e detalhado, prescrição médica do tratamento e acompanhamento mensal e completo.
							</p>
							<p>
								Se você ainda tem dúvidas sobre o procedimento, agende já sua consulta com o Dr. Wendell Uguetto!
							</p>

						</div>
					</div>		
				</div>
			</div>
			<div class="col-sm-12 col-xs-12 col-md-3 col-lg-3 submenu">
				<div class="text-uppercase indice-title"><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/tratamento-para-ginecomastia/">Tratamentos da Ginecomastia: </div>
					<ul>
						<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-tratamento-medicamentoso/">Tratamento Medicamentoso</a></li>
						<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/cirurgia-ginecomastia/" class="active">Cirugia Ginecomastia </a></li>
						<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-pre-operatorio/">Pré-Operatório</a></li>
						<li class="has-sublist">
							Procedimentos
							<ul class="sublist">
								<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/cirurgia-ginecomastia-lipoaspiracao/">Lipoaspiração</a></li>
								<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/cirurgia-ginecomastia-incisao-webster/">Incisão de webster</a></li>
								<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/cirurgia-ginecomastia-incisao-periareolar-circular/">Incisão Peri-Areolar Circular</a></li>
								<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/cirurgia-ginecomastia-incisao-periareolar-prolongamento-medial-lateral/">Incisão Peri-Areolar + Incisão no Tórax </a></li>
								<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/cirurgia-ginecomastia-incisao-periareolar-sulco-inframamario/">Incisão Peri-Areolar + Incisão submamária</a></li>
							</ul>
						</li>
						<li>
							<a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-pos-operatorio/">Pós-Operatório</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</section>

	<section class="mais">
		<div class="container">
			<div class="row">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 articles">
						<h2 class="section-title article-title">Conheça mais sobre Ginecomastia</h2>
						<div class="row">
							<?php
							include 'includes/partials/o-que-e.php';
							include 'includes/partials/graus.php';
							include 'includes/partials/causas.php';
							include 'includes/partials/tratamento.php';
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php 
	include 'ask.php';
	include 'footer.php';
	?>
