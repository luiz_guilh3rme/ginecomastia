<?php

// function compress_html($buffer) {
// 	$search = array(
//         '/\>[^\S ]+/s', // strip whitespaces after tags, except space
//         '/[^\S ]+\</s', // strip whitespaces before tags, except space
//         '/(\s)+/s'       // shorten multiple whitespace sequences
//     );
// 	$replace = array(
// 		'>',
// 		'<',
// 		'\\1'
// 	);
// 	$buffer = preg_replace($search, $replace, $buffer);
// 	return $buffer;
// }
// ob_start("compress_html"); 
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo $title; ?></title>
	<?php 
	if ( $bodyClass == 'interna success' || $bodyClass == 'interna error-page' )  {
		echo '<meta name="robots" content="noindex, nofollow">';
	}
	else {
		echo '<meta name="robots" content="index, follow">';
	}
	?>
	<base href="./">
	<meta name="description" content="<?php echo $description; ?>">
	<link rel="canonical" href="<?php echo $cannonical; ?>" />
	<link href="/favico.ico" rel="shortcut icon">
	<link rel="stylesheet" href="dist/main.css">    
		<!-- <link rel="stylesheet" href="/dist/main.css">  prod  -->
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-WDFDR8S');</script>
	<!-- End Google Tag Manager -->
	<!-- OG -->
	<meta property="og:image" content="https://www.ginecomastiatratamento.com.br/css/assets/logo-fundo-escuro.jpg" />
    <!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window, document,'script',
	'https://www.connect.facebook.net/en_US/fbevents.js');
	fbq('init', '224973384582514');
	fbq('track', 'PageView');
	</script>
	<!-- pixel -->
	<noscript>
	<img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=224973384582514&ev=PageView&noscript=1"/>
	</noscript>
	<!-- End Facebook Pixel Code -->
</head>
<body class="<?php echo $bodyClass; ?>">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WDFDR8S"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
	<!-- <script type="text/javascript" async src="https://www.d335luupugsy2.cloudfront.net/js/loader-scripts/e1eb0593-f97a-4c7f-b506-170dcb958b7b-loader.js"></script> -->
	<header>
		<div class="container">
			<div class="row">
				<nav class="navbar navbar-default">
					<div class="container-fluid">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle collapsed menu-abrir">
								<span class="sr-only">Menu Principal</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<a class="navbar-brand" href="https://www.ginecomastiatratamento.com.br/">
								<img itemprop="logo" src="https://www.ginecomastiatratamento.com.br/css/assets/logo-dr-wendell-uguetto.png" alt="Logo Dr. Wendell Uguetto" class="logo">
								<img itemprop="logo" src="https://www.ginecomastiatratamento.com.br/css/assets/logo-dr-wendell-uguetto-mobile.png" alt="Logo Dr. Wendell Uguetto" class="logo-mobile">
							</a>
						</div>
						<div class="navbar-collapse" id="navbar-principal">

							<ul class="nav navbar-nav">
								<li class="home"><a href="https://www.ginecomastiatratamento.com.br/">HOME</a></li>
								<li><a href="https://www.ginecomastiatratamento.com.br/dr-wendell-uguetto/">SOBRE O DR.</a></li>
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
										GINECOMASTIA
										<span class="caret"></span>
									</a>
									<ul class="dropdown-menu">
										<li><a href="https://www.ginecomastiatratamento.com.br/o-que-e-ginecomastia/">o que é?</a></li>
										<li><a href="https://www.ginecomastiatratamento.com.br/causas-da-ginecomastia/">causas</a></li>
										<li><a href="https://www.ginecomastiatratamento.com.br/graus-da-ginecomastia/">graus</a></li>
										<li><a href="https://www.ginecomastiatratamento.com.br/tratamento-para-ginecomastia/">tratamento</a></li>
									</ul>
								</li>
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
										CONSULTÓRIOS
										<span class="caret"></span>
									</a>
									<ul class="dropdown-menu">
										<li><a href="https://www.ginecomastiatratamento.com.br/consultorio-hospital-albert-einstein/">morumbi</a></li>
										<li><a href="https://www.ginecomastiatratamento.com.br/consultorio-itaim-bibi/">itaim bibi</a></li>
									</ul>
								</li>
                                <li>
									<a href="https://www.ginecomastiatratamento.com.br/blog">BLOG
									</a>
								</li>
							</ul>
							<div class="social">
								<div class="social-title">Siga-nos</div>
								<ul class="nav navbar-right">
									<li>
									<div class="agende">Agende uma consulta </div>
									<p class='tel-1'><a href="tel:1147501133" class="fone-topo">(11) 4750-1133</a></p>
									</li>
									<li><a href="https://pt-br.facebook.com/drwendelluguetto" target="_blank" rel="nofollow noopener" class="fa fa-facebook-square" title="Visite nossa página no Facebook"></a></li>
									<li><a href="https://www.linkedin.com/pub/wendell-fernando-uguetto/7b/2b9/9a6" target="_blank" rel="nofollow noopener" class="fa fa-linkedin" title="Visite nossa página no LinkedIn"></a></li>
									<li><a href="https://plus.google.com/+GinecomastiatratamentoBr/posts" target="_blank" rel="nofollow noopener" class="fa fa-google-plus" title="Visite nossa página no Google Plus" rel="publisher"></a></li>
								</ul>
							</div>

						</div>
					</div>
				</nav>
			</div>
		</div>
	</header>