<?php 
$bodyClass = 'interna';
$title = 'Ginecomastia Puberal | Ginecomastia Tratamento';
$description = 'Ginecomastia Puberal - É o desenvolvimento de mamas na adolescência do homem, fase de mudanças tão drásticas no corpo do menino. Conheça!';
$cannonical = 'https://www.ginecomastiatratamento.com.br/ginecomastia-puberal/';
$message = 'Entre em contato conosco';
$type = 'contato';
include 'header.php';

?>
<div itemscope itemtype="http://schema.org/WebPage">
	<div class="container">
		<div class="row">
		<div class="breadcrumb">
				<ul itemscope itemtype="http://schema.org/BreadcrumbList">
					<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
						<a itemprop="item" href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/"><i class="fa fa-home" ></i>
						<span itemprop="name">home</span>
						</a>
						<meta itemprop="position" content="1" />
					</li>
					<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
						<a itemprop="item" href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/causas-da-ginecomastia/">
						<span itemprop="name">Causas da Ginecomastia</span>
						</a>
						<meta itemprop="position" content="2" />
					</li>
					<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
						<span itemprop="name" class="active">Ginecomastia Puberal</span>
						<meta itemprop="position" content="3" />
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<section class="main-content">
	<div class="container">
		<div class="row row-border">
			<div class="content">
				<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">
					<h1 class="text-uppercase section-title text-blue">Ginecomastia Puberal</h1>
					<p>
						A Ginecomastia é muito comum na adolescência, atingindo 2 em cada 3 rapazes. Alguns estudos demonstram incidência ainda maior, de cerca de 87% dos adolescentes. Se inicia por volta dos 14 anos de idade e se caracteriza por um crescimento das mamas frequentemente assimétrico e atinge as duas mamas em 55% dos casos.
					</p>
					<p>
						Clinicamente, é possível observar a enfermidade através da presença de uma massa de tecido mamário abaixo da aréola, móvel e não aderida à pele e bastante dolorosa à palpação. Na prática clínica o caso mais típico é de um adolescente saudável, obeso ou com excesso de peso.
					</p>
				</div>
			</div>
			<div class="content">
				<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6"> 
					<div class="formulario">
						<?php include 'form-topo.php';?>
					</div>
					
				</div>	
			</div>
			
		</div>
		<div class="padding"></div>
		<div class="row">
			<div class="col-lg-9 row-border">
				<div class="col-sm-12 col-xs-12 col-lg-5 col-md-5">
					<div class="row">
						<div class="content">
							<h3 class="article-subtitle">Quais são as Causas?</h3>
							<p>
								A Ginecomastia do Adolescente tem causa fisiológica, ou seja, normal do organismo. Nesta fase de mudanças tão drásticas no corpo do menino, há um aumento transitório nos níveis de estradiol (hormônio Feminino) em relação à testosterona (hormônio Masculino), que estimulam os receptores mamários levando ao seu crescimento e desenvolvimento.
							</p>
							<p>
								Estudos científicos demonstram também maiores níveis de estrógenos em meninos com essa enfermidade quando comparados com meninos sem o problema.
							</p>
							<p>
								Porém, a ginecomastia do adolescente é transitória, autolimitada e na maioria das vezes desaparece entre 12 a 18 meses de evolução. Se o diâmetro for superior a 2cm, a regressão pode demorar até 2 anos. Ao fim de 2 anos pode ela pode persistir de forma residual numa ou em ambas as mamas por fibrose em até 8% dos casos. 
							</p>
						</div>
						
					</div>
				</div>
				<div class="col-sm-12 col-xs-12 col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1">
					<div class="row">
						<div class="content">
							<h3 class="article-subtitle">Níveis de tratamento</h3>
							<p>
								Na maioria dos casos o tratamento é expectante. O adolescente deve ser orientado do caráter autolimitado da doença. Ela deve desaparecer em até 1 ano e meio. Todavia, quando há impacto psicológico ou os sofrimentos de bulyng, o acompanhamento com um psicólogo pode ser necessário.
							</p>
							<p>
								Outra base importante é através do tratamento medicamentoso. Ele é indicado quando as dores não melhoram com o uso de analgésicos comuns ou quando limitam atividades básicas do dia a dia do adolescente. Estes medicamentos aceleram a regressão da ginecomastia e promovem a melhora da dor em até 75% dos casos.
							</p>
							<p>
								Em casos de ginecomastias maiores, chamadas de macroginecomastia, sejam fisiológicas ou patológicas, que não regredirem espontaneamente ou com medicamentos, serão necessários outros tratamentos, em geral cirúrgicos.
							</p>
						</div>
						
					</div>		
				</div>
			</div>
			<div class="col-sm-12 col-xs-12 col-md-3 col-lg-3 submenu">
				<div class="text-uppercase indice-title"><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/causas-da-ginecomastia/">Causas da Ginecomastia:</a></div>
				<ul>
					<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-idiopatica/">Ginecomastia Idiopática</a></li>
					<li>
						<a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-fisiologica/">
							Ginecomastia Fisiológica
						</a>
						<ul class="sublist">
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-puberal/" class="active">Ginecomastia Puberal</a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-senil/">Ginecomastia Senil</a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-neonatal/">Ginecomastia Neonatal</a></li>
						</ul>
					</li>
					<li>
						<a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-patologica/">
							Ginecomastia Patológica
						</a>
						<ul class="sublist">
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-patologica-nao-tumorais/">Ginecomastia Não-Tumoral</a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-patologica-tumoral/">Ginecomastia Tumoral</a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-causas-medicamentosas/">Ginecomastia Medicamentosa</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>

</section>

<section class="mais">
	<div class="container">
		<div class="row">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 articles">
					<h2 class="section-title article-title">Conheça mais sobre Ginecomastia</h2>
					<div class="row">
						<?php
						include 'includes/partials/o-que-e.php';
						include 'includes/partials/graus.php';
						include 'includes/partials/causas.php';
						include 'includes/partials/tratamento.php';
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<?php 
include 'ask.php';
include 'footer.php';
?>