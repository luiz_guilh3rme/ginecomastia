<?php 
$bodyClass = 'unidade';
$title = 'Unidade | Ginecomastia Tratamento';
$description = '';
$type = 'consulta';
include 'header.php';
$message = 'Agende uma consulta';
?>
<section class="principal">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 unidade">
				<h1><span class="help-block">UNIDADE MORUMBI  </span><span class="subtitle">ALBERT EINSTEIN</span></h1>
				<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
					<p>
						Além do consultório particular, Dr. Wendell atende seus pacientes em um consultório que pertence ao complexo hospitalar Albert Einstein, um dos hospitais mais sofisticados e uma referência na área da saúde com um todo.
					</p>
				</div>
				
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 formulario"> 
			<?php include 'form-topo.php'; ?>
			</div>
		</div>
	</div>
</section>
<section class="endereco">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-5 col-md-offset-1 col-lg-5 col-lg-offset-1">
				<h2>Endereço</h2>
				<p>
					Hospital Israelita Albert Einstein
				</p>
				<p>
					Av. Albert Einstein, 627. Bl A1. Consultório 119.
					CEP: 05652-900. São Paulo - SP
				</p>
				<h2>Entre em contato</h2>
				<p>
					(11) 4750-1133
				</p>
				<p>
					contato@ginecomastiatratamento.com.br
				</p>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<div id="map-canvas"></div>
			</div>
		</div>
	</div>
</section>

<?php 
include 'footer.php';
?>