<?php 
$bodyClass = 'interna';
$title = 'Cirurgia com Incisão Medial e Lateral | Dr. Wendell Uguetto';
$description = 'Cirurgia de Ginecomastia: Incisão Periareolar com Prolongamento Medial e Lateral (No Tórax) - Esta técnica permite retirar grandes quantidades de pele!';
$cannonical = 'https://www.ginecomastiatratamento.com.br/cirurgia-ginecomastia-incisao-periareolar-prolongamento-medial-lateral/';
$message = 'Entre em contato conosco';
$type = 'contato';
include 'header.php';

?>
<div itemscope itemtype="http://schema.org/WebPage">
	<div class="container">
		<div class="row">
		<div class="breadcrumb">
				<ul itemscope itemtype="http://schema.org/BreadcrumbList">
					<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
						<a itemprop="item" href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/"><i class="fa fa-home" ></i>
						<span itemprop="name">home</span>
						</a>
						<meta itemprop="position" content="1" />
					</li>
					<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
						<a itemprop="item" href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/tratamento-para-ginecomastia/">
						<span itemprop="name">Tratamentos da Ginecomastia</span>
						</a>
						<meta itemprop="position" content="2" />
					</li>
					<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
						<span itemprop="name" class="active">Cirurgia de Ginecomastia Incisão Periareolar com Prolongamento Medial e Lateral (No Tórax)</span>
						<meta itemprop="position" content="3" />
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<section class="main-content">
	<div class="container">
		<div class="row row-border">
			<div class="content">
				<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">
					<h1 class="text-uppercase section-title text-blue">Cirurgia de Ginecomastia Incisão Periareolar com Prolongamento Medial e Lateral (No Tórax)</h1>
					<p>
						Esse método cirúrgico permite redução do tamanho, melhor posicionamento e simetrização da aréola além de restaurar o contorno anatômico do tórax masculino. Como resultante final, o paciente obterá uma cicatriz circular periareolar e uma extensão medial e lateral transtorácica. 
					</p>

				</div>
				<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6"> 
					<div class="formulario">
						<?php include 'form-topo.php';?>
					</div>		
				</div>
			</div>
		</div>
		<div class="padding"></div>
		<div class="row">
			<div class="col-lg-9 row-border">
				<div class="col-sm-12 col-xs-12 col-lg-5 col-md-5">
					<div class="row">
						<div class="content">
							<h2 class="article-subtitle">Procedimentos da <span class="text-bold">Peri-Areolar Medial e Lateral</span></h2>
							<p>
								O paciente deve ser marcado em posição ortostática (de pé) por onde se definem a posição correta da aréola e a quantidade de pele a ser retirada que é na forma de um losango. A cirurgia pode ser iniciada por lipoaspiração para retirada do componente adiposo mamário. 
							</p>
							<p>
								Em seguida é feita uma incisão circular em torno da aréola, a qual pode ter seu tamanho reduzido, e outra incisão na região do losango. A área entre as duas incisões é desepidermizada e é confeccionado um pedículo vertical para a manutenção da vascularicação do complexo aréolo-papilar. O tecido mamario é identificado, dissecado e retirado e os vasos sanguíneos são cauterizados. Um dreno de sucção à vácuo pode ser posicionado. 
							</p>
							<p>
								Logo após, dá-se o fechamento das incisões e a resultante final é uma cicatriz em torno da aréola mais uma cicatriz horizontal que passa medialmente e lateralmente a ela.
							</p>

						</div>

					</div>
				</div>
				<div class="col-sm-12 col-xs-12 col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1">
					<div class="row">
						<div class="content">
							<h2 class="article-subtitle">Benefícios da <span class="text-bold">Peri-Areolar Medial e Lateral</span></h2>
							<p>
								Esta técnica permite retirar grandes quantidades de pele e é excelente para restaurar o contorno mamário adequado masculine. O único inconveniente é a presença de uma cicatriz horizontal que não se esconde em linhas anatômicas do tórax.
							</p>
							<h2 class="article-subtitle">Em quais casos aplica o método cirúrgico <span class="text-bold">Peri-Areolar Medial e Lateral?</span></h2>
							<p>
								A técnica de incisão periareolar circular com extensão medial e lateral é indicada para ginecomastia grau 3 de Simon, em que há grandes excessos de pele e de glândula mamária. Ela é indicada em ptoses moderadas, onde a posição da aréola não se encontra abaixo do sulco infra mamário.
							</p>
						</div>

					</div>		
				</div>
			</div>
			<div class="col-sm-12 col-xs-12 col-md-3 col-lg-3 submenu">
				<div class="text-uppercase indice-title"><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/tratamento-para-ginecomastia/">Tratamentos da Ginecomastia: </div>
				<ul>
					<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-tratamento-medicamentoso/">Tratamento Medicamentoso</a></li>
					<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/cirurgia-ginecomastia/">Cirugia Ginecomastia </a></li>
					<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-pre-operatorio/">Pré-Operatório</a></li>
					<li class="has-sublist">
						Procedimentos
						<ul class="sublist">
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/cirurgia-ginecomastia-lipoaspiracao/">Lipoaspiração</a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/cirurgia-ginecomastia-incisao-webster/">Incisão de webster</a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/cirurgia-ginecomastia-incisao-periareolar-circular/">Incisão Peri-Areolar Circular</a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/cirurgia-ginecomastia-incisao-periareolar-prolongamento-medial-lateral/" class="active">Incisão Peri-Areolar + Incisão no Tórax </a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/cirurgia-ginecomastia-incisao-periareolar-sulco-inframamario/">Incisão Peri-Areolar + Incisão submamária</a></li>
						</ul>
					</li>
					<li>
						<a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-pos-operatorio/">Pós-Operatório</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>

<section class="mais">
	<div class="container">
		<div class="row">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 articles">
					<h2 class="section-title article-title">Conheça mais sobre Ginecomastia</h2>
					<div class="row">
						<?php
						include 'includes/partials/o-que-e.php';
						include 'includes/partials/graus.php';
						include 'includes/partials/causas.php';
						include 'includes/partials/tratamento.php';
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php 
include 'ask.php';
include 'footer.php';
?>