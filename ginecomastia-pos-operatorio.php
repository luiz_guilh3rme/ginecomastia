<?php 
$bodyClass = 'interna';
$title = 'Ginecomastia Pós-Operatório | Ginecomastia Tratamento';
$cannonical = 'https://www.ginecomastiatratamento.com.br/ginecomastia-pos-operatorio/';
$description = 'Ginecomastia Pós-Operatório - O pós-operatório da cirurgia de ginecomastia geralmente é pouco doloroso. Conheça os cuidados que devem ser tomados!';
$message = 'Entre em contato conosco';
$type = 'contato';
include 'header.php';

?>
<div itemscope itemtype="http://schema.org/WebPage">
	<div class="container">
		<div class="row">
		<div class="breadcrumb">
				<ul itemscope itemtype="http://schema.org/BreadcrumbList">
					<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
						<a itemprop="item" href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/"><i class="fa fa-home" ></i>
						<span itemprop="name">home</span>
						</a>
						<meta itemprop="position" content="1" />
					</li>
					<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
						<a itemprop="item" href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/tratamento-para-ginecomastia/">
						<span itemprop="name">Tratamentos da Ginecomastia</span>
						</a>
						<meta itemprop="position" content="2" />
					</li>
					<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
						<span itemprop="name" class="active">Ginecomastia Pós-Operatório</span>
						<meta itemprop="position" content="3" />
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<section class="main-content">
	<div class="container">
		<div class="row row-border">
			<div class="content">
				<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">
					<h1 class="text-uppercase section-title text-blue">Ginecomastia Pós-Operatório</h1>
					<p>
						Assim que o procedimento cirúrgico é finalizado, o paciente é vestido com uma malha compressiva a qual é mantida por 1 a 2 meses. Os drenos, quando foram necessários, devem permanecer por no mínimo de 48 horas para a drenagem de secreções.
					</p>
					
				</div>
				<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6"> 
					<div class="formulario">
						<?php include 'form-topo.php';?>
					</div>		
				</div>
			</div>
		</div>
		<div class="padding"></div>
		<div class="row">
			<div class="col-lg-9 row-border">
				<div class="col-sm-12 col-xs-12 col-lg-5 col-md-5">
					<div class="row">
						<div class="content">
							<h4 class="article-subtitle">Incômodos do pós-operatório</h4>
							<p>
								O pós operatório da cirurgia de ginecomastia geralmente é pouco doloroso. Mas é comum, nas duas primeiras semanas, a região torácica apresentar equimose (coloração arroxeada) e ficar sensível ao toque. Um certo grau de sensibilidade é observado também na região das aréolas, que pode perdurar por até alguns meses após a cirurgia.
							</p>
							<h4 class="article-subtitle">Complicações do pós-operatório</h4>
							<p>
								A intercorrência mais comum no pós operatório imediato é o hematoma devido a algum vaso sanguíneo que voltou a sangrar. Caso ocorra, o paciente deve ser levado novamente ao centro cirúrgico, onde os coágulos são retirados e os vasos sanguíneos que sangraram são cauterizados novamente.
								Embora rara, a necrose parcial ou total de aréola pode acontecer e os riscos aumentam nos casos de grandes ressecções glandulares e cutâneas. Caso ocorra, a região necrótica pode ser tratada com curativos locais ou desbridamento cirúrgico com posterior reconstrução do complexo areolo-papilar.
							</p>

						</div>
						
					</div>
				</div>
				<div class="col-sm-12 col-xs-12 col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1">
					<div class="row">
						<div class="content">
							<h4 class="article-subtitle">Processo de cicatrização durante o pós-operatório</h4>
							<p>
								O processo total da cicatrização demora cerca de um ano, nesta fase as cicatrizes podem apresentar uma coloração avermelhada e palpação endurecida. Após 6 a 12 meses após o procedimento, as cicatrizes se tornam, paulatinamente, mais discretas. Problemas de cicatrização como quelóides ou cicatrizes hipertróficas são inerentes da cicatrização de cada indivíduo, e caso ocorram devem ter seu tratamento instituído o mais precoce possível.
							</p>
							<h4 class="article-subtitle">Recomendações do pós-operatório</h4>
							<p>
								As atividades habituais de trabalho e estudo são liberadas em cerca de 5 dias em casos de ginecomastias menores e em até 2 semanas para casos maiores. O paciente pode voltar a dirigir em 20 dias do procedimento. Atividades físicas moderadas como corridas leves, andar de bicicleta e treinos de musculação leves na academia são liberados por volta de 1 mês, enquanto atividades físicas intensas, a partir de 2 meses.
							</p>
						</div>
						
					</div>		
				</div>
			</div>
			<div class="col-sm-12 col-xs-12 col-md-3 col-lg-3 submenu">
				<div class="text-uppercase indice-title"><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/tratamento-para-ginecomastia/">Tratamentos da Ginecomastia: </div>
				<ul>
					<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-tratamento-medicamentoso/">Tratamento Medicamentoso</a></li>
					<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/cirurgia-ginecomastia/">Cirugia Ginecomastia </a></li>
					<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-pre-operatorio/">Pré-Operatório</a></li>
					<li class="has-sublist">
						Procedimentos
						<ul class="sublist">
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/cirurgia-ginecomastia-lipoaspiracao/">Lipoaspiração</a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/cirurgia-ginecomastia-incisao-webster/">Incisão de webster</a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/cirurgia-ginecomastia-incisao-periareolar-circular/">Incisão Peri-Areolar Circular</a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/cirurgia-ginecomastia-incisao-periareolar-prolongamento-medial-lateral/">Incisão Peri-Areolar + Incisão no Tórax </a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/cirurgia-ginecomastia-incisao-periareolar-sulco-inframamario/">Incisão Peri-Areolar + Incisão submamária</a></li>
						</ul>
					</li>
					<li>
						<a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-pos-operatorio/" class="active">Pós-Operatório</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>

<section class="mais">
	<div class="container">
		<div class="row">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 articles">
					<h2 class="section-title article-title">Conheça mais sobre Ginecomastia</h2>
					<div class="row">
						<?php
						include 'includes/partials/o-que-e.php';
						include 'includes/partials/graus.php';
						include 'includes/partials/causas.php';
						include 'includes/partials/tratamento.php';
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php 
include 'ask.php';
include 'footer.php';
?>
