<?php 
$bodyClass = 'interna';
$title = 'Ginecomastia Causas Medicamentosas | Ginecomastia Tratamento';
$description = 'Ginecomastia Causas Medicamentosas - O uso de esteroides, anabolizantes, heroína, maconha podem causar o desenvolvimento de mamas masculinas. Confira!';
$cannonical = 'https://www.ginecomastiatratamento.com.br/ginecomastia-causas-medicamentosas/';
$message = 'Entre em contato conosco';
$type = 'contato';
include 'header.php';

?>
<div itemscope itemtype="http://schema.org/WebPage">
	<div class="container">
		<div class="row">
		<div class="breadcrumb">
				<ul itemscope itemtype="http://schema.org/BreadcrumbList">
					<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
						<a itemprop="item" href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/"><i class="fa fa-home" ></i>
						<span itemprop="name">home</span>
						</a>
						<meta itemprop="position" content="1" />
					</li>
					<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
						<a itemprop="item" href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/causas-da-ginecomastia/">
						<span itemprop="name">Causas da Ginecomastia</span>
						</a>
						<meta itemprop="position" content="2" />
					</li>
					<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
						<span itemprop="name" class="active">Ginecomastia Medicamentosa</span>
						<meta itemprop="position" content="3" />
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<section class="main-content">
	<div class="container">
		<div class="row row-border">
			<div class="content">
				<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">
					<h1 class="text-uppercase section-title text-blue">Ginecomastia causas medicamentosas</h1>

				</div>
				<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6"> 
					<div class="formulario">
						<?php include 'form-topo.php';?>
					</div>		
				</div>
			</div>
		</div>
		<div class="padding"></div>
		<div class="row">
			<div class="col-lg-9 row-border">
				<div class="col-sm-12 col-xs-12 col-lg-5 col-md-5">
					<div class="row">
						<div class="content">
							<h3 class="article-subtitle">Com ação estrogênica </h3>
						<p>
							A administração de estrógenos ou de compostos com atividade estrogênica induzem a ginecomastia severa. Um exemplo típico é o desenvolvimento acentuado de mamas nos homens com carcinoma da próstata pós terapia hormonal para tratamento da doença. Nestes casos, a aplicação de radioterapia mamária preventiva foi bem sucedida em impedir o desenvolvimento da enfermidade.
						</p>
						<p>
							Usar digitálicos (medicação usada na Insuficiência cardíaca) também induz no desenvolvimento da Ginecomastia pela atividade estrogênica do medicamento.
						</p>
						<p>
							O clomifeno começou a ser usado recentemente para a infertilidade masculina, o seu uso assim como a descontinuação podem induzir ginecomastia por mecanismos incertos.
						</p>
						<p>
							O abuso de drogas, na forma de heroína injetável, esteróides anabolizantes ou cannabis (marijuana) fumado pode induzir ginecomastia pela depressão androgénica que exercem. Apesar do mecanismo não ser bem claro, evidenciou-se que o tetrahidrocabinol (metabolito da marijuana) tem uma estrutura quase semelhante ao estradiol. 
						</p>
						</div>
						
					</div>
				</div>
				<div class="col-sm-12 col-xs-12 col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1">
					<div class="row">
						<div class="content">
							<p>
							No caso dos anabolizantes, a testosterona contida em excesso em suas formulações é convertida em estrógenos pela ação da aromatase.
						</p>
						<h3 class="article-subtitle">Inibição da ação ou síndrome de testosterona </h3>
						<p>
							A espironolactona (um diurético usado para tratar hipertensão) quando usada regularmente e em altas doses causa frequentemente Ginecomastia, pois interfere na produção da testosterona. 
						</p>
						<p>
							Alguns antibióticos como o Metronidazol, antifúngicos como o Cetoconazol e medicamentos para tratar a gastrite como a Cimetidina são causas potenciais de ginecomastia.
						</p>
						<h3 class="article-subtitle">Aumento da Síntese de estrógenos pelos Testículos </h3>
						<p>
							A produção de estrógenos pelos testículos é induzida pelos hormônios gonadotropinas. Assim sendo, qualquer hormona gonadotrópica exerce um aumento da síntese de estrogeneos pelos testículos. Por exemplo, os tumores secretores de hCG, ou a própria hCG terapêutica.
						</p>
						</div>
						
					</div>		
				</div>
			</div>
			<div class="col-sm-12 col-xs-12 col-md-3 col-lg-3 submenu">
				<div class="text-uppercase indice-title"><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/causas-da-ginecomastia/">Causas da Ginecomastia:</a></div>
				<ul>
					<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-idiopatica/">Ginecomastia Idiopática</a></li>
					<li>
						<a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-fisiologica/">
							Ginecomastia Fisiológica
						</a>
						<ul class="sublist">
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-puberal/">Ginecomastia Puberal</a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-senil/">Ginecomastia Senil</a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-neonatal/">Ginecomastia Neonatal</a></li>
						</ul>
					</li>
					<li>
						<a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-patologica/">
							Ginecomastia Patológica
						</a>
						<ul class="sublist">
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-patologica-nao-tumorais/">Ginecomastia Não-Tumoral</a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-patologica-tumoral/">Ginecomastia Tumoral</a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-causas-medicamentosas/" class="active">Ginecomastia Medicamentosa</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>

</div>
</section>
<section class="mais">
	<div class="container">
		<div class="row">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 articles">
					<h2 class="section-title article-title">Conheça mais sobre Ginecomastia</h2>
					<div class="row">
						<?php
						include 'includes/partials/o-que-e.php';
						include 'includes/partials/graus.php';
						include 'includes/partials/causas.php';
						include 'includes/partials/tratamento.php';
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<?php 
include 'ask.php';
include 'footer.php';
?>
