<?php 
$bodyClass = 'interna error-page';
$title = '404 | Ginecomastia Tratamento';
$description = '';
$cannonical = '';
include 'header.php';

?>
<section class="main-content">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<div class="img-close"><img src="<?='http://'.$_SERVER["HTTP_HOST"] ?>/css/assets/close.svg"  class="img-resposive" alt=""></div>
				<div class="title text-uppercase">Erro 404</div>
				<div class="text-error text-center text-uppercase">página não encontrada! </div>
				<a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/" class="btn saibamais text-uppercase">Voltar para home</a>
			</div>
		</div>
	</div>
</section>
<?php include 'footer.php'; ?>