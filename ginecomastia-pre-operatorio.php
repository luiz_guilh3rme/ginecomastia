<?php 
$bodyClass = 'interna';
$title = 'Ginecomastia Pré-Operatório | Ginecomastia Tratamento';
$description = 'Ginecomastia Pré-Operatório - Uma avaliação clínica pré-operatória criteriosa deve ser feita a fim de reduzir riscos. Agende sua consulta!';
$cannonical = 'https://www.ginecomastiatratamento.com.br/ginecomastia-pre-operatorio/';
$message = 'Entre em contato conosco';
$type = 'contato';
include 'header.php';

?>
<div itemscope itemtype="http://schema.org/WebPage">
	<div class="container">
		<div class="row">
			<div class="breadcrumb">
				<ul itemscope itemtype="http://schema.org/BreadcrumbList">
					<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
						<a itemprop="item" href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/"><i class="fa fa-home" ></i>
							<span itemprop="name">home</span>
						</a>
						<meta itemprop="position" content="1" />
					</li>
					<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
						<a itemprop="item" href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/tratamento-para-ginecomastia/">
							<span itemprop="name">Tratamentos da Ginecomastia</span>
						</a>
						<meta itemprop="position" content="2" />
					</li>
					<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
						<span itemprop="name" class="active">Ginecomastia Pré-Operatório</span>
						<meta itemprop="position" content="3" />
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<section class="main-content">
	<div class="container">
		<div class="row row-border">
			<div class="content">
				<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">
					<h1 class="text-uppercase section-title text-blue">Ginecomastia Pré-Operatório</h1>
					<p>
						A Ginecomastia é o desenvolvimento anormal de mamas em homens. Essa condição masculina é resultante do aumento do volume mamário que pode ser de componente glandular (glândula mamária), adiposo (gordura) ou misto. <a href="https://www.ginecomastiatratamento.com.br/o-que-e-ginecomastia/">Leia mais!</a>
					</p>
					<p>
						Os candidatos à cirurgia são os pacientes com ginecomastia instalada há mais de 12 a 18 meses sem regressão espontânea, e que apresentam causa idiopática ou fisiológica. No entanto, nos pacientes com causa patológica ou medicamentosa é preciso tratar o fator causal para depois ser avaliada a possibilidade de cirurgia.
					</p>


					<h2 class="article-subtitle">Diagnóstico da <span class="text-bold">Ginecomastia</span></h2>
					<p>
						O diagnóstico da ginecomastia é basicamente clínico, mas exames de imagem como a mamografia e ultrassonografia de mamas são úteis na diferenciação da ginecomastia verdadeira da lipomastia (ou pseudoginecomastia) e também na pesquisa de nódulos e exclusão de malignidade.
					</p>

					<h2 class="article-subtitle">Como é feita a <span class="text-bold">avaliação?</span></h2>
					<p>
						Uma avaliação clínica pré-operatória criteriosa deve ser feita a fim de reduzir riscos de eventos cardiológicos, anestésicos e tromboembólicos, sempre pensando na segurança durante o ato operatório. Cabe ao especialista classificar seu tipo e grau de ginecomastia e explicar como será o tratamento, técnica proposta de cirurgia e cicatrizes resultantes.
					</p>
				</div>

				<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6"> 
					<div class="formulario">
						<?php include 'form-topo.php';?>
					</div>		
				</div>

				<div class="col-xs-12 col-sm-12 col-lg-12 col-md-12"> 
					<div class="content">
						<h2 class="article-subtitle">Solicitações para a <span class="text-bold">avaliação</span></h2>
						<p>
							Na avaliação clínica para a cirurgia de ginecomastia solicitamos exames laboratoriais de sangue para avaliar os níveis de hemoglobina, função renal e hepática, coagulação do sangue e glicemia. Raio X de tórax e eletrocardiograma são essenciais para avaliação cardíaca e pulmonar. Em pacientes com doenças de base, como hipertensão, diabetes, entre outras, exames complementares podem ser necessários.
						</p>
						<p>
							Algumas medicações devem ser interrompidas por no mínimo 15 dias antes do procedimento. Como é proibido usar qualquer tipo de droga ou medicação sem orientação médica neste mesmo período.
						</p>
						<p>
							No dia da cirurgia é necessário o jejum de alimentos sólidos e líquidos por no mínimo 8 horas. Chegue no Hospital com pelo menos 2 horas de antecedência do horário agendado e leve roupas confortáveis para a alta.
						</p>
					</div>

					<div class="content">
						<h2 class="article-subtitle">Como escolher uma clínica especializada em <span class="text-bold">Ginecomastia?</span></h2>
						<ul>
							<li>Escolha uma clínica de Cirurgia Plástica conceituada;</li>
							<li>Marque uma consulta com um Cirurgião Plástico;</li>
							<li>Faça a intervenção cirúrgica somente num hospital equipado com UTI;</li>
							<li>Histórico de seus profissionais; </li>
							<li>Medicamentos que serão utilizados durante o processo.</li>
						</ul>
					</div>

					<div class="content">
						<h2 class="article-subtitle">Procure um <span class="text-bold">Profissional Adequado</span></h2>
							<p>Alguns dos médicos que poderão lhe auxiliar neste momento são os de formação em clínica geral, mastologia, pediatria, urologia, entre outros. No entanto, com o Dr. Wendell Uguetto, você vai receber todo o atendimento necessário, através de um exame completo e detalhado, prescrição médica do tratamento e acompanhamento mensal e completo.</p>
							<p>Se você ainda tem dúvidas sobre o procedimento, agende já sua consulta com o Dr. Wendell Uguetto!</p>	
					</div>


				</div>
			</div>

		</div>

	</section>

	<section class="mais">
		<div class="container">
			<div class="row">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-9 articles">
						<h2 class="section-title article-title">Conheça mais sobre Ginecomastia</h2>
						<div class="row">
							<?php
							include 'includes/partials/o-que-e.php';
							include 'includes/partials/graus.php';
							include 'includes/partials/causas.php';
							include 'includes/partials/tratamento.php';
							?>
						</div>
					</div>



					<div class="col-xs-12 col-sm-12 col-lg-3 col-md-12 submenu"> 
						<div class="text-uppercase indice-title"><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/tratamento-para-ginecomastia/">Tratamentos da Ginecomastia: </div>
							<ul>
								<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-tratamento-medicamentoso/">Tratamento Medicamentoso</a></li>
								<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/cirurgia-ginecomastia/">Cirugia Ginecomastia </a></li>
								<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-pre-operatorio/" class="active">Pré-Operatório</a></li>
								<li class="has-sublist">
									Procedimentos
									<ul class="sublist">
										<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/cirurgia-ginecomastia-lipoaspiracao/">Lipoaspiração</a></li>
										<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/cirurgia-ginecomastia-incisao-webster/">Incisão de webster</a></li>
										<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/cirurgia-ginecomastia-incisao-periareolar-circular/">Incisão Peri-Areolar Circular</a></li>
										<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/cirurgia-ginecomastia-incisao-periareolar-prolongamento-medial-lateral/">Incisão Peri-Areolar + Incisão no Tórax </a></li>
										<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/cirurgia-ginecomastia-incisao-periareolar-sulco-inframamario/">Incisão Peri-Areolar + Incisão submamária</a></li>
									</ul>
								</li>
								<li>
									<a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-pos-operatorio/">Pós-Operatório</a>
								</li>
							</ul>
						</div>

					</div>
				</div>
			</div>
		</section>




		<?php 
		include 'ask.php';
		include 'footer.php';
		?>