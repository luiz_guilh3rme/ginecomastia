<?php 
$bodyClass = 'interna';
$title = 'Causas da Ginecomastia | Ginecomastia Tratamento';
$description = 'Causas da Ginecomastia - A ginecomastia pode ocorrer em pessoas de todas as idades: em recém-nascidos, adolescentes e adultos. Conheça todas suas causas!';
$cannonical = 'https://www.ginecomastiatratamento.com.br/causas-da-ginecomastia/';
$message = 'Entre em contato conosco';
$type = 'contato';
include 'header.php';

?>
<div itemscope itemtype="http://schema.org/WebPage">
	<div class="container">
		<div class="row">
		<div class="breadcrumb">
				<ul itemscope itemtype="http://schema.org/BreadcrumbList">
					<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
						<a itemprop="item" href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/"><i class="fa fa-home" ></i>
						<span itemprop="name">home</span>
						</a>
						<meta itemprop="position" content="1" />
					</li>
					<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
						<span itemprop="name" class="active">Causas da Ginecomastia</span>
						<meta itemprop="position" content="2" />
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<section class="main-content">
	<div class="container row-border">
		<div class="row ">
			<div class="content">
				<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">
					<h1 class="text-uppercase section-title">Causas da <span class="help-block">GINECOMASTIA</span></h1>
					<p>
						São muitas as causas da ginecomastia e elas podem variar conforme a idade do paciente. Em recém-nascidos, por exemplo, uma das causas do aparecimento é uma reação do corpo do bebê ao estrogênio da mãe – contato este ao longo de toda a gravidez.

					</p>
					<p>
						O surgimento da Ginecomastia pode ocorrer por três caminhos totalmente distintos, onde a falta de cuidado com o corpo ou a genética podem se tornar grandes fatores para o desenvolvimento do problema.

					</p>

					<p>
						Nos adolescentes, entre os 12 e 15 anos, a ginecomastia pode acontecer como sinal de que o aumento dos níveis de testosterona está atrasado no organismo. Tendo muito estrogênio ainda no corpo, ambas as mamas podem começar a se desenvolver, sendo essencial procurar um acompanhamento para reverter o quadro.
						
					</p>

					<p>
						Já para os adultos mais velhos, a ginecomastia costuma estar associada também a níveis inferiores de testosterona no organismo. É necessário procurar um profissional a fim de começar a consumir as dosagens certas do hormônio para ter o problema reduzido.
					</p>


				</div>
			</div>


			<div class="content">
				<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6 right"> 
					<div class="formulario">
						<?php include 'form-topo.php';?>
					</div>
					
				</div>
			</div>
			<div class="content">
				<div class="col-xs-12 col-sm-12 col-lg-12 col-md-12 ">
					<p>
						Vale destacar que existem alguns casos em que a testosterona acaba sendo diminuída no corpo do homem graças a condições de saúde ou até mesmo situações controversas. Confira as principais:
					</p>

					<ul>
						<li>Ampla exposição e contato ao estrogênio – hormônio feminino;</li>
						<li>Defeitos de cunho congênito;</li>
						<li>Doenças hepáticas, principalmente crônicas;</li>
						<li>Efeitos e reações a medicamentos;</li>
						<li>Hipertireoidismo;</li>
						<li>Insuficiência renal;</li>
						<li>Tratamento contra o câncer – quimioterapia;</li>
						<li>Tratamentos de cunho hormonal para portadores de câncer de próstata;</li>
						<li>Tratamentos que se utilizam de radiação na área dos testículos;</li>
						<li>Utilização ou exposição a hormônios esteroides e anabolizantes.</li>

					</ul>

					<p>
						Reparar em toda e qualquer mudança do corpo é indispensável para que o diagnóstico seja feito. Aliás, é essencial a procura por um profissional da saúde, tão logo se suspeite, visto que, quanto mais cedo a doença for descoberta, mais rápido e fácil será o tratamento também.
					</p>

						<h2 class="text-uppercase section-title">Procure um <span class="help-block">Profissional Adequado</span></h2>

						<p>
							Alguns dos médicos que poderão lhe auxiliar neste momento são os de formação em clínica geral, mastologia, pediatria, urologia, entre outros. No entanto, com o Dr. Wendell Uguetto, você vai receber todo o atendimento necessário, através de um exame completo e detalhado, prescrição médica do tratamento e acompanhamento mensal e completo.
						</p>

						<p>
							Se você ainda tem dúvidas sobre o procedimento, agende já sua consulta com o Dr. Wendell Uguetto!
						</p>




				</div> 
			</div>
			


		</div>
		<div class="row content">
			<div class="col-sm-12 col-xs-12 col-md-4 col-lg-4 text-center">
				<article class="content-causas">
					<h2 class="article-subtitle">Causas <span class="text-bold">Fisiológicas</span></h2>
					<p>
						A causa é chamada de fisiológica porque não há um fator causal responsável pela ginecomastia, como por exemplo uma doença ou medicamento, mas sim alterações do próprio organismo e que são normais para cada fase da vida.
					</p>
					<a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-fisiologica/" class="btn saibamais btn-article">SAIBA MAIS</a>
				</article>
			</div>
			<div class="col-sm-12 col-xs-12 col-md-4 col-lg-4 text-center">
				<article class="content-causas">
					<h2 class="article-subtitle">Causas <span class="text-bold">Idiopáticas</span></h2>
					<p>
						A grande maioria das ginecomastias não tem uma causa totalmente conhecida, sendo chamada de Idiopática. Ela acontece por conta de alterações em nível molecular que não foram ainda desvendados pela medicina atual.
					</p>
					<a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-idiopatica/" class="btn saibamais btn-article">SAIBA MAIS</a>
				</article>
			</div>
			<div class="col-sm-12 col-xs-12 col-md-4 col-lg-4 text-center">
				<article class="content-causas">
					<h2 class="article-subtitle">Causas <span class="text-bold">Patológicas</span></h2>
					<p>
						A ginecomastia é patológica quando há um fator causador conhecido, como doenças sistêmicas de base, tumores, uso de medicamentos específicos ou drogas que provocam um desarranjo hormonal que culminam com o desenvolvimento anormal de mamas nos homens.
					</p>
					<a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-patologica/" class="btn saibamais btn-article">SAIBA MAIS</a>
				</article>
			</div>
		</div>
	</div>
</section>
<section class="mais">
	<div class="container">
		<div class="row">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 articles">
					<h2 class="section-title article-title">Conheça mais sobre Ginecomastia</h2>
					<div class="row">
						<?php
						include 'includes/partials/o-que-e.php';
						include 'includes/partials/graus.php';
						include 'includes/partials/cirurgia.php';
						include 'includes/partials/medicamento.php';
						?>						
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<?php 
include 'ask.php';
include 'footer.php';
?>