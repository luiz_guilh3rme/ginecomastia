<?php 
$bodyClass = 'interna';
$title = 'Ginecomastia Tratamento Medicamentoso | Ginecomastia Tratamento';
$description = 'Ginecomastia Tratamento Medicamentoso - O tratamento por medicamentos é eficaz dependendo do grau da doença. Confira em quais casos são indicados!';
$cannonical = 'https://www.ginecomastiatratamento.com.br/ginecomastia-tratamento-medicamentoso/';
$message = 'Entre em contato conosco';
$type = 'contato';
include 'header.php';

?>
<div itemscope itemtype="http://schema.org/WebPage">
	<div class="container">
		<div class="row">
		<div class="breadcrumb">
				<ul itemscope itemtype="http://schema.org/BreadcrumbList">
					<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
						<a itemprop="item" href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/"><i class="fa fa-home" ></i>
						<span itemprop="name">home</span>
						</a>
						<meta itemprop="position" content="1" />
					</li>
					<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
						<a itemprop="item" href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/tratamento-para-ginecomastia/">
						<span itemprop="name">Tratamentos da Ginecomastia</span>
						</a>
						<meta itemprop="position" content="2" />
					</li>
					<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
						<span itemprop="name" class="active">Ginecomastia Tratamento Medicamentoso</span>
						<meta itemprop="position" content="3" />
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<section class="main-content">
	<div class="container">
		<div class="row row-border">
			<div class="content">
				<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">
					<h1 class="text-uppercase section-title text-blue">Ginecomastia Tratamento Medicamentoso</h1>
					<p>
						O tratamento medicamentoso é indicado quando a hipertrofia mamária é dolorosa, incapacitante e não melhora com analgésicos comuns. É indicado também quando provoca constrangimentos de ordem psicossocial como por exemplo evitar de tirar camiseta em público. Deve ser instituído o mais rápido possível, pois a Ginecomastia com mais de 1 ano e meio de progressão não responderá bem ao uso de medicamentos, fazendo-se necessário a correção cirúrgica.
					</p>

						<h2 class="article-subtitle">Classes de <span class="text-bold">tratamento</span> </h2>
							<p>
								Existem 3 classes de medicamentos que são indicadas para tratamento da ginecomastia: andrógenos (testosterona, dihidrotestosterona, danazol), inibidores de aromatase (anastrozole, letrozole) e anti-estrogênicos (clomifeno, tamoxifeno, raloxifeno).
							</p>
							<h2 class="subtitle">Andrógeno (Testosterona)</h2>
							<p>
								Antigamente acreditava-se que a administração de testosterona seria eficiente no tratamento da ginecomastia, mas descobriu-se que sua utilidade é extremamente limitada devido à conversão para estradiol com conseqüente piora da ginecomastia.
							</p>
					
				</div>
				<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6"> 
					<div class="formulario">
						<?php include 'form-topo.php';?>
					</div>		
				</div>
			</div>
		</div>
		<div class="padding"></div>
		<div class="row">
			<div class="col-lg-9 row-border">
				<div class="col-sm-12 col-xs-12 col-lg-12 col-md-12">
					<div class="row">
						<div class="content">
						
							<h2 class="subtitle">Andrógeno (Dhidrotestosterona)</h2>
							<p>
								A dihidrotestosterona (DHT) é um poderoso andrógeno que pode ser administrado em base de gel com absorção pela pele. O sucesso no tratamento da ginecomastia é intermediário, com média de 25% de regressão total e 50% de regressão parcial após 4 a 20 semanas de utilização do gel.
							</p>
							<h2 class="subtitle">Andrógeno (Danazol)</h2>
							<p>
								Atualmente, o danazol – um fraco androgênio – tem sido usado no tratamento da ginecomastia. Ele reduz a produção testicular de estrógenos e aumenta a quantidade de testosterona livre. Estudos mostram regressão completa da ginecomastia em 40% dos casos e melhora da dor em até 75%. Infelizmente, efeitos colaterais como edema, acne e outros limitam seu uso.
							</p>
							
						</div>
						
					</div>
				</div>
				<div class="col-sm-12 col-xs-12 col-lg-12 col-md-12 ">
					<div class="row">
						<div class="content">
							<h2 class="subtitle">Inibidores de Aromatase</h2>
							<p>
								Inibidores da enzima responsável pela conversão da testosterona em estrogênio, a aromatase, têm sido usados com diferentes graus de sucesso. A testolactona causa poucos efeitos colaterais e promove redução do tamanho das mamas, mas muito raramente leva à regressão completa.
							</p>
							<p>
								O mesmo acontece com a nova geração de inibidores de aromatase – anastrozole, letrozole. Também são eficientes na redução parcial da ginecomastia, mas quando comparados ao tamoxifeno, no entanto, demonstraram baixa eficácia.
							</p>
							<h2 class="subtitle">Antiestrogênico</h2>
							<p>
								Devido ao baixo índice de efeitos colaterais, custo e aliado ao fato de haver inúmeros estudos a respeito, os antiestrogênicos tem sido as drogas mais utilizadas no tratamento da ginecomastia.
							</p>
							<p>
								O tamoxifeno, um antiestrogênico com grande afinidade pelas células mamárias têm demonstrado grande eficácia no tratamento da ginecomastia, principalmente quando está no começo.
							</p>
							<p>
								Alguns estudos demonstram eficácia de quase 100% na ginecomastia puberal (do adolescente) e de até 78% em outros casos de ginecomastia. Atualmente, o tamoxifeno é a droga de escolha para o tratamento da ginecomastia idiopática com menos de 12 meses de história.
							</p>
							<p>
								Recentemente pesquisadores compararam o tamoxifeno com outro antiestrogênico menos conhecido – o raloxifeno. Alguma melhora foi vista em 86% dos pacientes que usaram o tamoxifeno e em 91% daqueles que fizeram uso do raloxifeno, contudo as melhoras mais significativas ocorreram no grupo que utilizou o raloxifeno, demonstrando seu uso pode ser promissor no futuro.
							</p>
						</div>


						<div class="content">
							<h2 class="subtitle">Como escolher uma especializada em Ginecomastia?</h2>
								<br>
								<ul>
									<li>Escolha uma clínica de Cirurgia Plástica conceituada;</li>
									<li>Marque uma consulta com um Cirurgião Plástico;</li>
									<li>Faça a intervenção cirúrgica somente num hospital equipado com UTI;</li>
									<li>Histórico de seus profissionais; </li>
									<li>Medicamentos que serão utilizados durante o processo.</li>
								</ul>
						</div>

					
						<div class="content">
							<h2 class="subtitle">Procure um profissional adequado</h2>
							<br>
								<p>
									Alguns dos médicos que poderão lhe auxiliar neste momento são os de formação em clínica geral, mastologia, pediatria, urologia, entre outros. No entanto, com o Dr. Wendell Uguetto, você vai receber todo o atendimento necessário, através de um exame completo e detalhado, prescrição médica do tratamento e acompanhamento mensal e completo.
								</p>
								<p>
									Se você ainda tem dúvidas sobre o procedimento, agende já sua consulta com o Dr. Wendell Uguetto!
								</p>
						</div>

						
					</div>		
				</div>
			</div>
			<div class="col-sm-12 col-xs-12 col-md-3 col-lg-3 submenu">
				<div class="text-uppercase indice-title"><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/tratamento-para-ginecomastia/">Tratamentos da Ginecomastia: </div>
				<ul>
					<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-tratamento-medicamentoso/" class="active">Tratamento Medicamentoso</a></li>
					<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/cirurgia-ginecomastia/">Cirugia Ginecomastia </a></li>
					<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-pre-operatorio/">Pré-Operatório</a></li>
					<li class="has-sublist">
						Procedimentos
						<ul class="sublist">
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/cirurgia-ginecomastia-lipoaspiracao/">Lipoaspiração</a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/cirurgia-ginecomastia-incisao-webster/">Incisão de webster</a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/cirurgia-ginecomastia-incisao-periareolar-circular/">Incisão Peri-Areolar Circular</a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/cirurgia-ginecomastia-incisao-periareolar-prolongamento-medial-lateral/">Incisão Peri-Areolar + Incisão no Tórax </a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/cirurgia-ginecomastia-incisao-periareolar-sulco-inframamario/">Incisão Peri-Areolar + Incisão submamária</a></li>
						</ul>
					</li>
					<li>
						<a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-pos-operatorio/">Pós-Operatório</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>

<section class="mais">
	<div class="container">
		<div class="row">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 articles">
					<h2 class="section-title article-title">Conheça mais sobre Ginecomastia</h2>
					<div class="row">
						<?php
						include 'includes/partials/o-que-e.php';
						include 'includes/partials/graus.php';
						include 'includes/partials/causas.php';
						include 'includes/partials/tratamento.php';
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php 
include 'ask.php';
include 'footer.php';
?>