<?php

$bodyClass = 'unidade';

$title = 'Consultório Itaim Bibi | Ginecomastia Tratamento';

$description = 'Dr. Wendell Uguetto atende seus pacientes em um consultório particular totalmente equipado, que fica localizado no Itaim Bibi. Agende sua consulta!';

$cannonical = 'https://www.ginecomastiatratamento.com.br/consultorio-itaim-bibi/';

$type = 'consulta';

include 'header.php';

$message = 'Agende uma consulta';

?>

<script>

	lat=-23.5932516;

	long=-46.6815992;

	mensagem = "<div class='title'>Consultório Itaim Bibi</div>Rua Professor Atílio Innocenti, 683Vila Nova Conceição,<br>  São Paulo - SP. CEP: 04538-001";

</script>

<div itemscope itemtype="http://schema.org/WebPage">

	<div class="container">

		<div class="row">

			<div class="breadcrumb">

				<ul itemscope itemtype="http://schema.org/BreadcrumbList">

					<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">

						<a itemprop="item" href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/"><i class="fa fa-home" ></i>

							<span itemprop="name">home</span>

						</a>

						<meta itemprop="position" content="1" />

					</li>

					<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">

						<span itemprop="name" class="active">Consultório Itaim Bibi</span>

						<meta itemprop="position" content="2" />

					</li>

				</ul>

			</div>

		</div>

	</div>

</div>

<section class="main-content">

	<div class="container">

		<div class="row row-border">

			<div class="content endereco">

				<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">

					<h1 class="text-uppercase section-title text-blue">Consultório Itaim Bibi</h1>

					<p>

						Além do consultório particular, Dr. Wendell atende seus pacientes em um consultório que pertence ao complexo hospitalar Albert Einstein, um dos hospitais mais sofisticados e uma referência na área da saúde com um todo.

					</p>

					<div id="map-canvas"></div>

					<h2 class="section-subtitle">Endereço</h2>



					<div itemscope itemtype="http://schema.org/Organization">

						<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">

							<p>

								<span itemprop="streetAddress">Rua Professor Atílio Innocenti, 683 Vila Nova Conceição</span><br>

								<span itemprop="postalCode">CEP: 04538-001</span>. <span itemprop="addressLocality">São Paulo - SP</span><br>

							</div>

						</div>

					</p>

				</div>

				<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">

					<div class="formulario formulario-itaim">

						<?php include('form-unidade.php') ?>

					</div>

					<h2 class="section-subtitle">Entre em contato</h2>

					<p class="text-bold">

						<span itemprop="telephone" class="tel-1">(11) 4750-1133</span><br>

						<span itemprop="email"><a href="mailto:contato@ginecomastiatratamento.com.br">contato@ginecomastiatratamento.com.br</a></span>

					</p>

				</div>

			</div>

		</div>

	</div>

</section>

<!-- Remarketing -->

<script type="text/javascript">



/* <![CDATA[ */



var google_conversion_id = 882336618;



var google_custom_params = window.google_tag_params;



var google_remarketing_only = true;



/* ]]> */



</script>



<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">



</script>



<noscript>



<div style="display:inline; position: absolute;">



<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/882336618/?value=0&amp;guid=ON&amp;script=0"/>



</div>



</noscript>

<?php

include 'footer.php';

?>

