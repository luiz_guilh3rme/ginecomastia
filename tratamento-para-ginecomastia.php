<?php 
$bodyClass = 'interna';
$title = 'Tratamento para Ginecomastia | Ginecomastia Tratamento';
$message = 'Entre em contato conosco';
$description = 'Tratamento para Ginecomastia - O aumento das mamas no homem pode ser tratado com medicamentos ou cirurgia. Conheças cada uma delas!';
$cannonical = 'https://www.ginecomastiatratamento.com.br/tratamento-para-ginecomastia/';
$type = 'contato';
include 'header.php';

?>
<div itemscope itemtype="http://schema.org/WebPage">
	<div class="container">
		<div class="row">
		<div class="breadcrumb">
				<ul itemscope itemtype="http://schema.org/BreadcrumbList">
					<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
						<a itemprop="item" href="<?='https://'.$_SERVER["HTTPS_HOST"] ?>/"><i class="fa fa-home" ></i>
						<span itemprop="name">home</span>
						</a>
						<meta itemprop="position" content="1" />
					</li>
					<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
						<span itemprop="name" class="active">Tratamentos para Ginecomastia</span>
						<meta itemprop="position" content="2" />
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<section class="main-content">
	<div class="container row-border">
		<div class="row">
			<div class="content">
				<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">
					<h1 class="text-uppercase section-title">Ginecomastia: <span class="help-block">TRATAMENTO</span></h1>
					<p>
						O tratamento para ginecomastia pode ser feito com uso de medicamentos ou cirurgia. Independente do grau de cada caso, o tratamento deve ser direcionado para combater a sua causa. Contudo, são diferentes.
					</p>
					<p>
						O medicamento serve para casos recentes e em estágios iniciais da doença. Já o cirúrgico é quando o problema passa a se tornar um grande incomodo, tanto estético, como psicológico. Estudos científicos demonstram que após o tratamento adequado da ginecomastia o paciente se sente mais confiante e com maior auto-estima.
					</p>
					<p>
						No caso da ginecomastia na adolescência, o tratamento nem sempre é necessário pois o aumento das mamas tende a desaparecer com o passar do tempo. Uma opção de tratamento natural para ginecomastia é optar por exercícios físicos que objetivam fortalecer o peitoral e emagrecer, queimando toda a gordura localizada. Além disso, os tratamento estéticos com aparelhos que eliminam a gordura e que melhoram a firmeza da pele também podem ser usados. No entanto, devem ser orientados pelo fisioterapeuta.
					</p>
				</div>
			</div>
			<div class="content">
				<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6 right"> 
					<div class="formulario">
						<?php include 'form-topo.php';?>
					</div>

				</div>
			</div>


	<div class="col-xs-12 col-sm-12 col-lg-13 col-md-12"> 
					<div class="content">
						<h2 class="article-subtitle">Afinal, qual tratamento que <span class="text-bold">realmente funciona?</span></h2>
						<p>
							O verdadeiro tratamento para Ginecomastia é a cirurgia de mama masculina. Essa cirurgia é simples, tanto que o procedimento tem duração de até 3 horas. E pode ser feito ainda na adolescência, desde que o paciente esteja em boas condições de saúde.
						</p>
						<p>
							Sendo assim, como escolher uma boa clínica para realizar a cirurgia de Ginecomastia?
						</p>
					</div>

					<div class="content">
						<ul>
							<li>Escolha uma clínica de Cirurgia Plástica conceituada;</li>
							<li>Marque uma consulta com um Cirurgião Plástico;</li>
							<li>Faça a intervenção cirúrgica somente num hospital equipado com UTI;</li>
							<li>Histórico de seus profissionais; </li>
							<li>Medicamentos que serão utilizados durante o processo.</li>
						</ul>
					</div>

					<div class="content">
						<h2 class="article-subtitle">Procure um <span class="text-bold">Profissional Adequado</span></h2>
							<p>Alguns dos médicos que poderão lhe auxiliar neste momento são os de formação em clínica geral, mastologia, pediatria, urologia, entre outros. No entanto, com o Dr. Wendell Uguetto, você vai receber todo o atendimento necessário, através de um exame completo e detalhado, prescrição médica do tratamento e acompanhamento mensal e completo.</p>
							<p>Se você ainda tem dúvidas sobre o procedimento, agende já sua consulta com o Dr. Wendell Uguetto!</p>	
					</div>


				</div>
			
			
		</div>
		<div class="row content">
			<div class="col-sm-12 col-xs-12 col-lg-6 col-md-6 text-center">
				<article class="content-causas">
					<h2 class="article-subtitle">Tratamento <span class="text-bold">Medicamentoso</span></h2>
					<p>
						O tratamento medicamentoso é indicado quando a ginecomastia é dolorosa e ou implica em alterações psicológicas e deve ser iniciado o quanto antes, uma vez que ginecomastias instaladas há mais de 12 a 18 meses não responderão bem ao uso de medicamentos, fazendo-se necessário a correção cirúrgica.
					</p>
					<a href="https://www.ginecomastiatratamento.com.br/ginecomastia-tratamento-medicamentoso/" class="btn saibamais btn-article tratamentos">SAIBA MAIS</a>
				</article>
				
			</div>
			<div class="col-sm-12 col-xs-12 col-lg-6 text-center">
				<article class="content-causas">
					<h2 class="article-subtitle">Tratamento <span class="text-bold">Cirúrgico</span></h2>
					<p>
						A cirurgia de Ginecomastia só deve ser realizada após o cuidado de eventuais causas subjacentes (como perda de peso, tratamento de tumores, suspensão do consumo de substâncias como hormônios, anabolizantes, medicamentos, álcool ou maconha). O objetivo do tratamento cirúrgico é devolver ao paciente um formato de tórax mais anatômico.
					</p>
					<a href="https://www.ginecomastiatratamento.com.br/cirurgia-ginecomastia/" class="btn saibamais text-center btn-article tratamentos">SAIBA MAIS</a>
				</article>
			</div>
		</div>
		
	</div>
</section>
<section class="mais">
	<div class="container">
		<div class="row">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 articles">
					<h2 class="section-title article-title">Conheça mais sobre Ginecomastia</h2>
					<div class="row">
						<?php
							include 'includes/partials/o-que-e.php';
							include 'includes/partials/graus.php';
							include 'includes/partials/causas.php';
							include 'includes/partials/cirurgia.php';
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php 
include 'ask.php';
include 'footer.php';
?>