<div class="call-cta-wrapper">
	<div class="tooltip">
		<p class="tooltip-text">
			<span class="variant">Olá!</span>
		Gostaria de receber uma ligação gratuita?</p>
		<button class="confirm tracked" data-category="CTA - Telefone" data-action="Click" data-label="Abriu CTA de Telefone">
			SIM
		</button>
	</div>
	<img src="https://www.ginecomastiatratamento.com.br/css/assets/images/callcta.png" aria-hidden="true" class="call-cta">
</div>  