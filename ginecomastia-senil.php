<?php 
$bodyClass = 'interna';
$title = 'Ginecomastia Senil | Ginecomastia Tratamento';
$description = 'Ginecomastia Senil - Aparece durante a andropausa (em homens com idade acima de 50 anos) com queda na produção dos hormônios masculinos. Saiba Mais!';
$cannonical = 'https://www.ginecomastiatratamento.com.br/ginecomastia-senil/';
$message = 'Entre em contato conosco';
$type = 'contato';
include 'header.php';

?>
<div itemscope itemtype="http://schema.org/WebPage">
	<div class="container">
		<div class="row">
		<div class="breadcrumb">
				<ul itemscope itemtype="http://schema.org/BreadcrumbList">
					<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
						<a itemprop="item" href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/"><i class="fa fa-home" ></i>
						<span itemprop="name">home</span>
						</a>
						<meta itemprop="position" content="1" />
					</li>
					<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
						<a itemprop="item" href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/causas-da-ginecomastia/">
						<span itemprop="name">Causas da Ginecomastia</span>
						</a>
						<meta itemprop="position" content="2" />
					</li>
					<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
						<span itemprop="name" class="active">Ginecomastia Senil</span>
						<meta itemprop="position" content="3" />
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<section class="main-content">
	<div class="container">
		<div class="row row-border">
			<div class="content">
				<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">
					<h1 class="text-uppercase section-title text-blue">Ginecomastia Senil</h1>
					<p>
						Cerca de 72% homens com idade entre 50 e 69 anos apresenta alguma forma de ginecomastia, em geral bilateral. Há uma elevação proporcional dos níveis de estrógenos em relação aos andrógenos que consequentemente estimulam o tecido mamário masculino e provocam o seu crescimento.
					</p>
					<p>
						Ao contrário dos adolescentes, geralmente não surge repentinamente com uma massa endurecida retroareolar, mas sim com crescimento gradual e indolor e em geral bilateral do tecido mamário.
					</p>
					<p>
						Clinicamente, a ginecomastia senil se apresenta como qualquer outra, através do aumento do volume das mamas de componente glandular e tecido gorduroso.
					</p>
				</div>
			</div>
			<div class="content">
				<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6"> 
					<div class="formulario">
						<?php include 'form-topo.php';?>
					</div>
					
				</div>	
			</div>
			
		</div>
		<div class="padding"></div>
		<div class="row">
			<div class="col-lg-9 row-border">
				<div class="col-sm-12 col-xs-12 col-lg-5 col-md-5">
					<div class="row">
						<div class="content">
							<h3 class="article-subtitle">Quais são as Causas?</h3>
							<p>
								Os mecanismos exatos ainda não foram totalmente descobertos, mas acredita-se que a Ginecomastia Senil seja decorrente de dois fatores principais. 
							</p>
							<p>
								A primeira é uma redução fisiológica, ou seja, normal com o envelhecimento, que resulta em diminuição na produção dos hormônios masculinos, dentre eles a testosterona. O segundo fator é secundário ao aumento da gordura corpórea total, elevando os níveis de estrógenos (hormônios femininos) livres no organismo.
							</p>
						</div>
						
					</div>
				</div>
				<div class="col-sm-12 col-xs-12 col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1">
					<div class="row">
						<div class="content">
							<p>
								Deve-se também investigar outras causas, como uso de medicamentos e doenças, que são mais frequentes com o envelhecimento. 
							</p>
							<h3 class="article-subtitle">Níveis de Tratamento</h3>
							<p>
								A progressão desta enfermidade pode ser freada com o emagrecimento, atividades físicas moderadas e reposição hormonal em casos selecionados. A ginecomastia instalada, que não desaparece entre 12 e 18 meses, tem indicação cirúrgica.
							</p>
						</div>
						
					</div>		
				</div>
			</div>
			<div class="col-sm-12 col-xs-12 col-md-3 col-lg-3 submenu">
				<div class="text-uppercase indice-title"><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/causas-da-ginecomastia/">Causas da Ginecomastia:</a></div>
				<ul>
					<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-idiopatica/">Ginecomastia Idiopática</a></li>
					<li>
						<a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-fisiologica/">
							Ginecomastia Fisiológica
						</a>
						<ul class="sublist">
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-puberal/">Ginecomastia Puberal</a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-senil/" class="active">Ginecomastia Senil</a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-neonatal/">Ginecomastia Neonatal</a></li>
						</ul>
					</li>
					<li>
						<a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-patologica/">
							Ginecomastia Patológica
						</a>
						<ul class="sublist">
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-patologica-nao-tumorais/">Ginecomastia Não-Tumoral</a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-patologica-tumoral/">Ginecomastia Tumoral</a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-causas-medicamentosas/">Ginecomastia Medicamentosa</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>

</section>

<section class="mais">
	<div class="container">
		<div class="row">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 articles">
					<h2 class="section-title article-title">Conheça mais sobre Ginecomastia</h2>
					<div class="row">
						<?php
						include 'includes/partials/o-que-e.php';
						include 'includes/partials/graus.php';
						include 'includes/partials/causas.php';
						include 'includes/partials/tratamento.php';
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<?php 
include 'ask.php';
include 'footer.php';
?>