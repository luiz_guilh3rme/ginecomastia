<?php
// $posts = get_recent_posts();
// if($posts): 
?>
<script>
window.onload = function() {

	function RequestFromBlog(settings) {

		this.settings = {
			baseURL 	:	'<?php echo $_SERVER['SERVER_NAME']; ?>' , 
			postsURL 	:  '/blog/wp-json/wp/v2/posts',
			mediaURL	: '/blog/wp-json/wp/v2/media/',
			per_page 	: 2, 
			debug		: false,
		}

		this.init();

	}

	RequestFromBlog.prototype.init = function() {

		if ( this.settings.debug ) {
			this.logSettings();
		}

		this.outputPosts();
	}

	RequestFromBlog.prototype.logSettings = function() {
		console.log(this.settings);
	}

	RequestFromBlog.prototype.makeAjaxCall = function (method, data, url) {
		return $.ajax({
			type: method,
			url: url,
			data: data,
		});
	}

	RequestFromBlog.prototype.getPosts = function() {
		var s = this.settings;
		var posts = this.makeAjaxCall('GET', '', s.postsURL + '?per_page=' + s.per_page );
		return posts;
	}

	RequestFromBlog.prototype.outputPosts = function() {
		var posts = this.getPosts();
		var s = this.settings;

		posts.complete(function(r){
			var fetchedPosts = r.responseJSON
			var status = r.status;
			var postHTML = ''; 

			if ( status == 200 ) {

				for ( var i = 0; i < fetchedPosts.length; i++  ) {
					var postName = fetchedPosts[i].title.rendered,
					postExcerpt = fetchedPosts[i].excerpt.rendered,
					postLink = fetchedPosts[i].link,
					postID = fetchedPosts[i].id,
					featuredImage = fetchedPosts[i].fimg_url !== undefined ? fetchedPosts[i].fimg_url : '//placehold.it/170x170';
					
					console.log(fetchedPosts[i]);
					
					postHTML += '<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">';
					postHTML += '<article>'; 
					
					postHTML += '<a href="'+postLink+'"><img src="'+featuredImage+'" alt="'+postName+'"></a>';
					postHTML += '<a href="'+postLink+'"><h3 class="section-subtitle">'+postName+'</h3></a>';
					postHTML += postExcerpt;
					
					postHTML += '</article>';
					postHTML += '</div>'; // col-xs-12 col-sm-12 col-md-6 col-lg-6
					
				}

			}
				
			$('.receive-posts').append(postHTML);

		});

		posts.fail(function(){
			$('.receive-posts').append('<p class="text-center mt-1 mb-1">Houve um problema na conexão com o nosso blog. Por favor atualize a página ou clique <a href="/blog/" target="_BLANK">AQUI</a> para acessar diretamente</p>');
		});
		
	}

	
	var request = new RequestFromBlog();

}
</script>
<section class="blog">
	<div class="container">
		<div class="row">
			<a target="_blank" href="<?='https://'.$_SERVER["HTTP_HOST"] ?>/blog">
				<h2 class="text-center text-uppercase section-title">BLOG <span>GINECOMASTIA</span></h2>
			</a>
			<div class="col-xs12 col-sm-12 col-md-12 col-lg-12 receive-posts"></div>
		</div>
	</div>
</section>
<?php
// endif;

function get_recent_posts($per_page = 2) {
//	featured_media

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $_SERVER['SERVER_NAME'] . "/blog/wp-json/wp/v2/posts?per_page={$per_page}");
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	$output = curl_exec($ch);
	$result = json_decode($output, true);
	curl_close($ch);

	//
	if(is_array($result) && !empty($result)):
		for($i=0; $i< count($result); $i++) {
			$result[$i]['featured_image_url'] = get_media($result[$i]["featured_media"]);
		}

		return $result;

	else:
		return false;
	endif;

}
function get_media($id) {

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $_SERVER['SERVER_NAME'] . "/blog/wp-json/wp/v2/media/{$id}");
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	$output = curl_exec($ch);
	$result = json_decode($output, true);
	curl_close($ch);

	if(is_array($result) && !empty($result)):
		if(isset($result["media_details"]["sizes"]["thumbnail"])):

			return $result["media_details"]["sizes"]["thumbnail"]["source_url"];
		else:
			return $result["guid"]["rendered"];
		endif;
	else:
		return false;
	endif;
}