<?php 
$bodyClass = 'interna';
$title = 'Ginecomastia Causas Não Tumorais | Ginecomastia Tratamento';
$description = 'Ginecomastia Causas Não Tumorais - Podemos citar a obesidade, a cirrose hepática, o hipertireoidismo, insuficiência renal, etc. Saiba Mais!';
$cannonical = 'https://www.ginecomastiatratamento.com.br/ginecomastia-patologica-nao-tumorais/';
$message = 'Entre em contato conosco';
$type = 'contato';
include 'header.php';

?>
<div itemscope itemtype="http://schema.org/WebPage">
	<div class="container">
		<div class="row">
		<div class="breadcrumb">
				<ul itemscope itemtype="http://schema.org/BreadcrumbList">
					<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
						<a itemprop="item" href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/"><i class="fa fa-home" ></i>
						<span itemprop="name">home</span>
						</a>
						<meta itemprop="position" content="1" />
					</li>
					<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
						<a itemprop="item" href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/causas-da-ginecomastia/">
						<span itemprop="name">Causas da Ginecomastia</span>
						</a>
						<meta itemprop="position" content="2" />
					</li>
					<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
						<span itemprop="name" class="active">Ginecomastia Não-Tumoral</span>
						<meta itemprop="position" content="3" />
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<section class="main-content">
	<div class="container">
		<div class="row row-border">
			<div class="content">
				<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">
					<h1 class="text-uppercase section-title text-blue">Ginecomastia causas não tumorais</h1>
					<p>
						Das causas não tumorais podemos citar a obesidade, a cirrose hepática, o hipertireoidismo, hipogonadismo (redução da função testicular), insuficiência renal e o eunuquismo como as origens mais comuns.
					</p>
				</div>
				<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6"> 
					<div class="formulario">
						<?php include 'form-topo.php';?>
					</div>		
				</div>
			</div>
		</div>
		<div class="padding"></div>
		<div class="row">
			<div class="col-lg-9 row-border">
				<div class="col-sm-12 col-xs-12 col-lg-5 col-md-5">
					<div class="row">
						<div class="content">
							<h3 class="article-subtitle">Obesidade</h3>
							<p>
								A obesidade causa um aumento da atividade da enzima aromatase no tecido adiposo, resultando na maior conversão de andrógenos em estrógenos. Como consequência há uma concentração maior de estrógenos circulantes na corrente sanguínea, os quais são os responsáveis pelo desenvolvimento do tecido mamário.
							</p>
							<h3 class="article-subtitle">Cirrose Hepática</h3>
							<p>
								Cerca de 40% dos homens que tem cirrose hepática sofre com ginecomastia. As alterações endócrinas são múltiplas e incluem uma combinação da insuficiência hepática com a diminuição da degradação dos estrógenos pelo fígado, uma insuficiência testicular auto-imune com consequente redução na produção de andrógenos e possivelmente um efeito sinérgico do consumo de álcool (nos casos de cirrose alcoólica).
							</p>
							<h3 class="article-subtitle">Hipertireoidismo</h3>
							<p>
								A doença de Graves é a forma mais comum de hipertireoidismo e há associação com ginecomastia de 20 a 40% dos casos. Clinicamente ocorre um aumento grosseiro, bilateral e doloroso das mamas e a função testicular é normal. Os níveis séricos dos estrógeneos são elevados com níveis de testosterona normais, mas a patogenia exata da ginecomastia ainda é desconhecida.
							</p>
							<h3 class="article-subtitle">Hipogonadismo</h3>
							<p>
								A síndrome de Klinefelter é a condição mais comum de hipogonadismo masculino primário, sendo decorrente de uma anomalia cromossômica com cariótipo 47 XXY, que resulta em hipodesenvolvimento das características sexuais masculinas, deficiência na produção de andrógenos e alterações nos espermatozóides.
							</p>
							<h3 class="article-subtitle">Outras Causas</h3>
							<p>
								Qualquer doença testicular adquirida que resulte em hipogonadismo primário como uma orquite viral ou bacteriana, um traumatismo ou irradiação também pode promover o aparecimento de ginecomastia pelos mesmos mecanismos. Ou seja, por redução na produção de andrógenos.
							</p>
						</div>
						
					</div>
				</div>
				<div class="col-sm-12 col-xs-12 col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1">
					<div class="row">
						<div class="content">
							<p>
								O problema da ginecomastia pode estar presente em até 90% dos pacientes, sendo geralmente bilateral e não se manifestando antes da puberdade. É importante manter alto grau de suspeita de Síndrome de Klinefelter em indivíduos de sexo masculino com ginecomastia e hipogonadismo hipergonadotrópico. 
							</p>
							<p>
								Seu tratamento médico é cirúrgico e minucioso, sendo que a ginecomastia é melhor tratada com a excisão do tecido mamário hipertrófico e preservação do complexo areolar, não somente por questões estéticas, mas por conta de uma demonstração científica, que elucidou o aumento de até 20 vezes no risco de câncer de mama nestes pacientes.
							</p>
							<h3 class="article-subtitle">Insuficiência Renal</h3>
							<p>
								Ela causa importante deterioração da função testicular por disfunção das células de Leydig. Nos doentes submetidos a diálise, há associação com ginecomastia em 50% dos casos. Os pacientes apresentam níveis baixos de testosterona com níveis de FSH e LH elevados.
							</p>
							<h3 class="article-subtitle">Eunuquismo</h3>
							<p>
								A anorquia congênita, ou eunuquismo, é uma regressão dos testículos na fase embrionária do bebê ainda na barriga da mãe. Os testículos estão ausentes no homem fenotipicamente masculino com um cariótipo normal 46, XY. Foi estimado que até metade dos indivíduos com anorquia desenvolvem ginecomastia.
							</p>
							<p>
								Qualquer doença testicular adquirida que resulte em hipogonadismo primário como uma orquite viral ou bacteriana, um traumatismo ou irradiação também pode promover o aparecimento de ginecomastia pelos mesmos mecanismos. Ou seja, por redução na produção de andrógenos.
							</p>
						</div>
						
					</div>		
				</div>
			</div>
			<div class="col-sm-12 col-xs-12 col-md-3 col-lg-3 submenu">
				<div class="text-uppercase indice-title"><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/causas-da-ginecomastia/">Causas da Ginecomastia:</a></div>
				<ul>
					<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-idiopatica/">Ginecomastia Idiopática</a></li>
					<li>
						<a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-fisiologica/">
							Ginecomastia Fisiológica
						</a>
						<ul class="sublist">
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-puberal/">Ginecomastia Puberal</a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-senil/">Ginecomastia Senil</a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-neonatal/">Ginecomastia Neonatal</a></li>
						</ul>
					</li>
					<li>
						<a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-patologica/">
							Ginecomastia Patológica
						</a>
						<ul class="sublist">
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-patologica-nao-tumorais/" class="active">Ginecomastia Não-Tumoral</a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-patologica-tumoral/">Ginecomastia Tumoral</a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-causas-medicamentosas/">Ginecomastia Medicamentosa</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>

<section class="mais">
	<div class="container">
		<div class="row">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 articles">
					<h2 class="section-title article-title">Conheça mais sobre Ginecomastia</h2>
					<div class="row">
						<?php
						include 'includes/partials/o-que-e.php';
						include 'includes/partials/graus.php';
						include 'includes/partials/causas.php';
						include 'includes/partials/tratamento.php';
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<?php 
include 'ask.php';
include 'footer.php';
?>
