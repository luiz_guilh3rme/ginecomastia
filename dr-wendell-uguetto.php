<?php



$bodyClass = 'quemsomos';

$title =	'Dr. Wendell Uguetto | Ginecomastia Tratamento';

$description = 'Dr. Wendell Uguetto atua como médico na área de cirurgia plástica desde 2006. Formado pela USP, é especialista em Ginecomastia. Conheça!';

$cannonical = 'https://www.ginecomastiatratamento.com.br/dr-wendell-uguetto/';

$message = "Entre em contato conosco!";

$type = 'contato';

include 'header.php';

?>

<div itemscope itemtype="http://schema.org/WebPage">

	<div class="container">

		<div class="row">

		<div class="breadcrumb">

				<ul itemscope itemtype="http://schema.org/BreadcrumbList">

					<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">

						<a itemprop="item" href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/"><i class="fa fa-home" ></i>

						<span itemprop="name">home</span>

						</a>

						<meta itemprop="position" content="1" />

					</li>

					<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">

						<span itemprop="name" class="active">Sobre o Dr. Wendell Uguetto</span>

						<meta itemprop="position" content="2" />

					</li>

				</ul>

			</div>

		</div>

	</div>

</div>



<section class="main-content">

	<div class="container row-border">

		<div class="row">

			<div class="content">

				<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">

					<h1 class="section-title text-uppercase">Sobre o Dr.<span class="help-block">Wendell Uguetto</h1>

					<p>

						Formado pela Faculdade de Medicina da Universidade de São Paulo (USP), Doutor Wendell Uguetto fez residência em Cirurgia Geral no Hospital das Clínicas da USP e foi aprovado em primeiro lugar na residência de Cirurgia Plástica no mesmo Hospital.

					</p>

					<p>

						Além disso, Uguetto participou do Grupo de Cirurgia de Nariz e de Cirurgia Crânio-Facial do Hospital das Clínicas da USP, onde se dedicou a casos complexos, na grande maioria oriundos de acidentes graves. Em paralelo dedicava-se à cirurgia estética. A busca incessante pela perfeição, bem como a experiência adquirida durante anos o levaram a receber em 2011 o Prêmio Quality de Cirurgião Plástico do ano.

					</p>



					<h2 class="section-subtitle">A vida de <span>Wendell Uguetto</span></h2>

					<p>

						Wendell é nascido em São Paulo. Aos 9 anos, sua família se mudou para Belém, no Pará, onde ficou até os 13 anos de idade. Depois sua família foi morar em Capivari, interior de São Paulo. Nessa época, Uguetto estudou no Anglo Capivari, onde terminou o ensino médio e conseguiu passar em 6 faculdades de medicina.

					</p>

					<p>

						Diante de tantas opções, Uguetto optou pela USP Pinheiros por ser a melhor faculdade de Medicina do país, realizando teu sonho. Dessa forma, aos 19 anos de idade, Wendell mudava novamente para São Paulo, onde mora até os dias atuais. Ele iniciou a faculdade em 1998 e se formou em 2003.

					</p>

					<p>

						Logo após o período acadêmico, Wendell fez cirurgia geral no Hospital das Clínicas da Faculdade de Medicina da USP, passando em primeiro lugar na residência de cirurgia plástica para o mesmo hospital. A concorrência para essa especialidade é altíssima. Somente pessoas com boas notas e bem capacitadas conseguem entrar. Ele iniciou esta especialização em 2006 e terminou em 2009.

					</p>

					<p>

						Terminando a residência de cirurgia plástica, Uguetto foi escolhido para trabalhar por um ano como médico preceptor no departamento de Cirurgia Plástica do HC da Faculdade de medicina da USP, onde era um dos responsáveis pelo ensino dos alunos e residentes. Ele esteve nesta função entre fevereiro de 2009 e o mesmo mês de 2010.

					</p>



				</div>

			</div>



			<div class="content">

			<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6 right">

					<div class="doutor-img">

						<img src="<?='http://'.$_SERVER["HTTP_HOST"] ?>/css/assets/dr-wendell-uguetto-em-seu-consultorio.jpg" alt="Dr. Wendell Uguetto em seu consultório" class="img-responsive">

					</div>



					<h2 class="section-subtitle"><span>Atualmente</span></h2>

					<p>

						Nos dias atuais, Wendell Uguetto dedica-se exclusivamente aos seus pacientes. Atende em dois consultórios. Um localizado na Vila Nova Conceição e outro nas dependências do hospital Albert Einstein.

					</p>

					<p>

						Junto com essas atividades, Uguetto é membro de equipe de retaguarda de cirurgia plástica e de cirurgia crânio-maxilo-facial do Hospital Albert Einstein. E, uma vez por mês, leciona na Faculdade de Medicina da Universidade São Francisco – USF em Bragança Paulista.

					</p>

				</div>

			</div>



		</div>

	</div>

</section>

<section class="ask">

	<div class="container">

		<div class="row">

			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 formulario">

				<?php include('form-pergunte.php') ?>

			</div>

			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-center">

				<div class="form-title">Siga nas redes socias</div>

				<ul class="social">

					<li><a href="https://www.facebook.com/drwendelluguetto"><i class="fa fa-facebook-square"></i>Facebook</a></li>

					<li><a href="https://www.linkedin.com/in/wendell-fernando-uguetto-9a62b97b"><i class="fa fa-linkedin"></i>Linkedin</a></li>

					<li><a href="https://plus.google.com/+GinecomastiatratamentoBr/posts"><i class="fa fa-google-plus"></i>Google +</a></li>

				</ul>

			</div>

		</div>

	</div>

</section>

<?php

include 'footer.php';

?>

