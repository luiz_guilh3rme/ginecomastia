<?php 
$bodyClass = 'interna';
$title = 'Graus da Ginecomastia | Ginecomastia Tratamento';
$description = 'Graus da Ginecomastia - A ginecomastia pode ser classificada em grau 1, 2 ou 3, de acordo com a quantidade e características do tecido mamário. Confira!';
$cannonical = 'https://www.ginecomastiatratamento.com.br/graus-da-ginecomastia/';
$message = 'Entre em contato conosco';
$type = 'contato';
include 'header.php';

?>
<div itemscope itemtype="http://schema.org/WebPage">
	<div class="container">
		<div class="row">
		<div class="breadcrumb">
				<ul itemscope itemtype="http://schema.org/BreadcrumbList">
					<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
						<a itemprop="item" href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/"><i class="fa fa-home" ></i>
						<span itemprop="name">home</span>
						</a>
						<meta itemprop="position" content="1" />
					</li>
					<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
						<span itemprop="name" class="active">Graus da Ginecomastia</span>
						<meta itemprop="position" content="2" />
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<section class="main-content">
	<div class="container row-border">
		<div class="row">
			<div class="content">
				<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">
					<h1 class="section-title text-uppercase text-blue">Graus da Ginecomastia</h1>
					<p>
						Diversas classificações foram propostas para a diferenciar a Ginecomastia. Destas, utilizamos 2 classificações, uma histológica e outra clínica.
					</p>
					<p>
						Em 1972, Bannayan e Hajdu classificaram a ginecomastia em três tipos histológicos:
					</p>
					<p>
					<ul>
						<li><strong>Tipo florido:</strong> é a forma ativa da ginecomastia, há intensa atividade de proliferação dos tecidos mamários. Tem curta duração, geralmente inferior a quatro meses.</li>
						<li><strong>Tipo fibroso:</strong> é a forma inativa da ginecomastia, as alterações do tecido mamário já se estabilizaram, com presença de fibrose. Mais comum após um ano de duração.</li>
						<li><strong>Tipo intermediário:</strong> é uma forma mista de ginecomastia, com áreas floridas e áreas fibróticas. Encontrado entre 5 e 12 meses de duração.</li>
					</ul>
					<p>
						Essa classificação é útil para entendermos que apenas ginecomastias em atividade, ou seja, do tipo florido ou intermediário terão resposta ao tratamento medicamentoso. Quando o tempo de evolução da ginecomastia é maior que 1 ano, teremos grande parte do tecido mamário do tipo fibroso, que não responde aos medicamentos, portanto o tratamento é de indicação cirúrgica.
					</p>
					<p>
						Para a classificação clínica, a grande maioria da literatura médica mundial utiliza a classificação proposta por Simon em 1973, devido a sua simplicidade de reprodução e por sua correlação clínico-cirúrgica. Tipicamente, considera-se uma classificação de quatro graus, que vai do mais leve, com uma hipertrofia mínima e sem sobra de pele ao mais grave, com um volume superior a 500 gramas e com ptose mamária acentuada.
					</p>
					<h2 class="section-subtitle">O que significa os Graus de Ginecomastia?</h2>

					<p>
						A ginecomastia não é um problema grave, porém, é motivo de bastante desconforto e problemas de autoestima e insegurança quanto à própria aparência. Trata-se de um aumento nas glândulas mamárias dos homens, desencadeado por desequilíbrios hormonais.
					</p>

					<p>
						Esse problema, ao contrário do que muitas pessoas pensam, não atinge exclusivamente os adolescentes. Na verdade, a condição pode ser comum em homens que chegam à terceira idade, por conta da queda na produção de testosterona, e também em recém-nascidos, ainda por conta de exposição ao estrogênio materno.
					</p>

					<p>
						O fato é que, independentemente da idade, a ginecomastia é uma condição desagradável que, quando não regride de forma espontânea, exige tratamentos medicamentosos ou cirúrgicos. A decisão acerca da melhor forma de tratamento tem a ver com o grau de evolução do quadro. Conheça as diferenças entre os principais estágios de desenvolvimento da ginecomastia:
					</p>


<h3 class="subtitle">Grau 1</h3>
					<p>
						O primeiro grau da ginecomastia é caracterizado por um aumento discreto nas mamas masculinas. A massa da glândula mamária não ultrapassa o valor de 250g. Apesar de ser um aumento pequeno, já é possível perceber que a mama fica com um aspecto “saltado”. Isso faz com que as camisetas mais justas fiquem marcadas, o que já gera um certo constrangimento. Nesses casos, não há sobra de pele. Quando o quadro não passa por regressão espontânea, é recomendado o tratamento medicamentoso.
					</p>
				

				</div>
			</div>
			<div class="content">
				<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6 right">
					<div class="formulario">
						<?php include 'form-topo.php';?>
					</div>

					

					<h3 class="subtitle">Grau 2 (A e B) </h3>
					<p>
						O grau 2a registra um aumento maior nas mamas, geralmente com massa entre 250g e 500g. Nesse estágio, o aumento mamário não fica apenas restrito às mamas propriamente ditas, pois já é possível notar um comprometimento em boa parte da região torácica. Isso faz com que as mamas sejam mais facilmente perceptíveis sob a roupa.O grau 2b possui basicamente as mesmas condições dos casos de ginecomastia em grau 2a. A diferença é que o estágio 2b, inclui um excesso de pele na região torácica, que não é verificado no grau 2a.
					</p>
					<h3 class="subtitle">Grau 3 </h3>
					<p>
						O grau 3 é o estágio mais avançado da ginecomastia. Os casos desse grau registram um aumento grande na massa das mamas, superior a 500g. Esse peso causa ptose mamária, um termo médico para mamas “caídas”. Nesse estágio, há sobras de pele que comprometem o aspecto geral do tórax, ficando difícil disfarçar a condição. Como o impacto psicológico tende a ser mais intenso nesse estágio, a cirurgia para remoção da glândula mamária é uma opção recomendável.
					</p>
					<p>
						Na maioria dos casos, a ginecomastia acomete as duas mamas (bilateral), mas, em algumas ocasiões, pode atingir apenas uma (unilateral). É importante que o médico avalie o grau evolutivo do problema para recomendar o melhor tratamento. Além de descartar causas adjacentes ou a possibilidade de regressão espontânea, também é preciso levar em consideração o estado emocional do indivíduo quanto ao problema que está enfrentando.
					</p>

					<h4 class="section-subtitle">Classificação da Ginecomastia (Simon, 1973)</h2>
					<p>
						<strong>Grau 1 -</strong> pequeno aumento da mama, sem sobra de pele
					</p>
					<p>
						<strong>Grau 2A </strong>- aumento moderado da mama, sem sobra de pele
					</p>
					<p>
						<strong>Grau 2B </strong>- aumento moderado da mama, com excesso de pele
					</p>
					<p>
						<strong>Grau 3 -</strong> grande aumento da mama, com excesso de pele
					</p>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
				<h4 class="section-subtitle">Procure um profissional adequado</h4>
<p>Alguns dos médicos que poderão lhe auxiliar neste momento são os de formação em clínica geral, mastologia, pediatria, urologia, entre outros. No entanto, com o Dr. Wendell Uguetto, você vai receber todo o atendimento necessário, através de um exame completo e detalhado, prescrição médica do tratamento e acompanhamento mensal e completo.</p>

<p>Se você ainda tem dúvidas sobre o procedimento, agende já sua consulta com o Dr. Wendell Uguetto!</p>

			</div>
			
		</div>
	</div>
</section>
<section class="mais">
	<div class="container">
		<div class="row">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 articles">
					<h2 class="section-title article-title">Conheça mais sobre Ginecomastia</h2>
					<div class="row">
						<?php
							include 'includes/partials/o-que-e.php';
							include 'includes/partials/causas.php';
							include 'includes/partials/tratamento.php';
							include 'includes/partials/cirurgia.php';
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php 
include 'ask.php';
include 'footer.php';
?>