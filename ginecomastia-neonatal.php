<?php 
$bodyClass = 'interna';
$title = 'Ginecomastia Neonatal | Ginecomastia Tratamento';
$description = 'Ginecomastia Neonatal - O aumento das mamas em recém nascidos ocorre devido a influência dos hormônios femininos da mãe. Conheça as causas e tratamentos!';
$cannonical = 'https://www.ginecomastiatratamento.com.br/ginecomastia-neonatal/';
$message = 'Entre em contato conosco';
$type = 'contato';
include 'header.php';

?>
<div itemscope itemtype="http://schema.org/WebPage">
	<div class="container">
		<div class="row">
		<div class="breadcrumb">
				<ul itemscope itemtype="http://schema.org/BreadcrumbList">
					<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
						<a itemprop="item" href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/"><i class="fa fa-home" ></i>
						<span itemprop="name">home</span>
						</a>
						<meta itemprop="position" content="1" />
					</li>
					<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
						<a itemprop="item" href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/causas-da-ginecomastia/">
						<span itemprop="name">Causas da Ginecomastia</span>
						</a>
						<meta itemprop="position" content="2" />
					</li>
					<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
						<span itemprop="name" class="active">Ginecomastia Neonatal</span>
						<meta itemprop="position" content="3" />
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<section class="main-content">
	<div class="container">
		<div class="row row-border">
			<div class="content">
				<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">
					<h1 class="text-uppercase section-title text-blue">Ginecomastia Neonatal</h1>
					<p>
						O aumento do volume das mamas que acontece nos recém nascidos é bastante comum. Também conhecida como Ginecomastia Neo-natal ou do Recém Nascido, ocorre pouco tempo depois do nascimento e está presente tanto no sexo masculino quanto no feminino.
					</p>

				</div>
			</div>
			<div class="content">
				<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6"> 
					<div class="formulario">
						<?php include 'form-topo.php';?>
					</div>
				</div>	
			</div>
		</div>
		<div class="padding"></div>
		<div class="row">
			<div class="col-sm-12 col-xs-12 col-md-3 col-lg-3 submenu">
				<div class="text-uppercase indice-title"><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/causas-da-ginecomastia/">Causas da Ginecomastia:</a></div>
				<ul>
					<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-idiopatica/">Ginecomastia Idiopática</a></li>
					<li>
						<a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-fisiologica/">
							Ginecomastia Fisiológica
						</a>
						<ul class="sublist">
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-puberal/">Ginecomastia Puberal</a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-senil/">Ginecomastia Senil</a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-neonatal/" class="active">Ginecomastia Neonatal</a></li>
						</ul>
					</li>
					<li>
						<a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-patologica/">
							Ginecomastia Patológica
						</a>
						<ul class="sublist">
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-patologica-nao-tumorais/">Ginecomastia Não-Tumoral</a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-patologica-tumoral/">Ginecomastia Tumoral</a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-causas-medicamentosas/">Ginecomastia Medicamentosa</a></li>
						</ul>
					</li>
				</ul>
			</div>
			<div class="col-md-9 row-border">
				<div class="col-sm-12 col-xs-12 col-lg-5 col-md-5">
					<div class="row">
						<div class="content">
							<h3 class="article-subtitle">Causa</h3>
							<p>
								Ela é causada pelos altos níveis de hormônios maternos (como o estradiol e a progesterona) que passaram pela placenta para o bebê no final da gestação e que estimulam o crescimento do tecido mamário dos recém nascidos.
							</p>
							<p>
								Clinicamente alguns bebês vão apresentar um nódulo endurecido abaixo da aréola enquanto outros, um aumento generalizado de todo o parênquima mamário. Esta enfermidade pode ser unilateral ou, na maioria dos casos acometer as duas mamas (bilateral), em alguns casos pode ser dolorosa ao toque.
							</p>
						</div>

					</div>

				</div>
				<div class="col-sm-12 col-xs-12 col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1">
					<div class="row">
						<div class="content">
							<p>
								Apesar de ser uma situação que deixa os pais bastante preocupados, este aumento mamário desaparece em poucas semanas e é considerado fisiológico e autolimitado, uma vez que gradativamente os hormônios maternos vão sendo eliminados pelo corpo do bebê.
							</p>
							<h3 class="article-subtitle">Níveis de tratamento</h3>
							<p>
								Não são necessários tratamentos adicionais, sendo apenas indicado o acompanhamento de rotina. Cabe ao pediatra tranquilizar os pais e nos casos em que a Ginecomastia Neonatal causar dores, indicar compressas mornas na região para aliviar este sintoma.
							</p>
						</div>

					</div>

				</div>		
			</div>
		</div>
	</div>
</section>
<section class="mais">
	<div class="container">
		<div class="row">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 articles">
					<h2 class="section-title article-title">Conheça mais sobre Ginecomastia</h2>
					<div class="row">
						<?php
						include 'includes/partials/o-que-e.php';
						include 'includes/partials/graus.php';
						include 'includes/partials/causas.php';
						include 'includes/partials/tratamento.php';
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<?php 
include 'ask.php';

include 'footer.php';
?>