<form id="form-unidade">
	<!-- NESP -->
	<input type="hidden" name="url" value="<?= "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>">
	<!-- NESP -->
	<input type="hidden" name="tipo" value="<?php echo $type;?>">
    <input type="hidden" name="identifier" value="Formulário Unidades">
	<div class="form-title text-uppercase">Fale com a Gente!</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">
			<div class="form-group">
				<input type="text" placeholder="Nome" class="form-control" name="nome" id="nome_unidade" required>
			</div>
			<div class="form-group">
				<input type="email" placeholder="e-mail" class="form-control" name="email" id="email_unidade" required>
			</div>
			<div class="form-group">
				<input type="text" placeholder="Telefone" class="form-control telefone" name="telefone" id="telefone_unidade" pattern="\([0-9]{2}\)[\s][0-9]{4,5}-[0-9]{4}" required>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">
			<div class="form-group">
				<input type="text" placeholder="Cidade" class="form-control" name="cidade" id="cidade_unidade" required>
			</div>
			<div class="form-group">
				<input type="text" placeholder="Estado" class="form-control" name="estado" id="estado_unidade" required>
			</div>
			<div class="form-group">
				<select class="form-control" name="plano-saude" id="plano_unidade">
					<option selected disabled>Qual seu plano de saúde?</option>
					<option value="Sul America">Sul America</option>
					<option value="Bradesco">Bradesco</option>
					<option value="Omint">Omint</option>
					<option value="Amil">Amil</option>
					<option value="Unimed">Unimed</option>
					<option value="OneHealth">OneHealth</option>
					<option value="Notre Dame">Notre Dame</option>
					<option value="MedService">MedService</option>
					<option value="Lincx">Lincx</option>
					<option value="Alianz Saúde">Alianz Saúde</option>
					<option value="CarePlus">CarePlus</option>
					<option value="Intermedica">Intermedica</option>
					<option value="Outro">Outro</option>
					<option value="Não possuo Plano">Não possuo Plano</option>
				</select>
			</div>
		</div>
	</div>	
	<div class="row">	
		<div class="col-xs-6 col-sm-6 col-lg-12 col-md-6 text-form">
			<div class="form-group">
				<select class="form-control" name="tempo-ginecomastia" id="tempo_unidade">
					<option selected disabled>Há quanto tempo tem ginecomastia?</option>
					<option value="A Mais de 1 Ano">A Mais de 1 Ano</option>
					<option value="A Menos de 1 Ano">A Menos de 1 Ano</option>
				</select>
			</div>
			<div class="form-group">
				<textarea 
				placeholder="Mensagem" class="form-control" rows="6" name="mensagem" id="mensagem_unidade" required></textarea>
			</div>
			<button class="btn" type="submit">ENVIAR</button>
		</div>
	</div>
</form>