<?php include 'blog.php'; ?>
<footer>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 menu footer-links">
					<div class="footer-title">SOBRE A DOENÇA</div>
					<ul>
						<li><a href="<?='https://'.$_SERVER["HTTP_HOST"] ?>/o-que-e-ginecomastia/">O que é</a></li>

						<li><a href="<?='https://'.$_SERVER["HTTP_HOST"] ?>/causas-da-ginecomastia/">Causas</a></li>

						<li><a href="<?='https://'.$_SERVER["HTTP_HOST"] ?>/tratamento-para-ginecomastia/">Tratamento</a></li>

						<li><a href="<?='https://'.$_SERVER["HTTP_HOST"] ?>/cirurgia-ginecomastia/">Cirurgia</a></li>

						<li><a href="<?='https://'.$_SERVER["HTTP_HOST"] ?>/graus-da-ginecomastia/">Graus da Doença</a></li>
					</ul>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 footer-links">
					<div class="footer-title text-uppercase">DR. WENDELL Uguetto</div>
					<p>
						Doutor Wendell Uguetto atua como médico na área de cirurgia plástica desde 2006. Formado pela USP e ganhador de
						vários prêmios.
					</p>
					<a href="<?='https://'.$_SERVER["HTTP_HOST"] ?>/dr-wendell-uguetto/">Saiba mais</a>
				</div>
				<div itemscope itemtype="https://schema.org/Organization">
					<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 footer-links">
						<div class="footer-title">UNIDADES</div>
						<p class="adress" itemscope itemtype="https://schema.org/LocalBusiness">
							<meta itemprop="name" content="Ginecomastia Tratamento" />
							<address itemprop="address" itemscope itemtype="https://schema.org/PostalAddress">
								<div class="footer-subtitle">ITAIM BIBI</div>
								<span itemprop="streetAddress">Rua Professor Atílio Innocenti, 683<br>
								Vila Nova Conceição,</span><span itemprop="addressLocality"> São Paulo</span> - SP<br>
								<span itemprop="postalCode">CEP: 04538-001</span>
							</address>
						</p>
						<p class="adress" itemscope itemtype="https://schema.org/LocalBusiness">
							<meta itemprop="name" content="Ginecomastia Tratamento" />
							<address itemprop="address" itemscope itemtype="https://schema.org/PostalAddress">
								<div class="footer-subtitle">MORUMBI</div>
								Hospital Israelita Albert Einstein <br>
								<span itemprop="streetAddress">Av. Albert Einstein, 627<br>
									Bloco A1, Consultório 119<br>
								Morumbi,</span> <span itemprop="addressLocality">São Paulo</span> - <span itemprop="addressRegion">SP</span><br>
							</address>
						</p>
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 social footer-links" itemscope itemtype="https://schema.org/Organization">
					<link itemprop="url" href="https://www.ginecomastiatratamento.com.br">
					<div class="footer-title">CONTATO</div>
					<ul>
						<li><a itemprop="sameAs" href="https://pt-br.facebook.com/drwendelluguetto" target="_blank" rel="nofollow noopener"
							class="fa fa-facebook-square" title="Visite nossa página no Facebook"></a></li>
							<li><a itemprop="sameAs" href="https://www.linkedin.com/pub/wendell-fernando-uguetto/7b/2b9/9a6" target="_blank"
								rel="nofollow noopener" class="fa fa-linkedin" title="Visite nossa página no Linkedin"></a></li>
								<li><a itemprop="sameAs" href="https://plus.google.com/+GinecomastiatratamentoBr/posts" target="_blank" rel="nofollow noopener"
									class="fa fa-google-plus" rel="publisher" title="Visite nossa página no Google Plus"></a></li>
								</ul>
								<div class="footer-title"><span itemprop="telephone"><a href="tel:1147501133" title="Ligue para nós" class="fone-footer">(11) 4750-1133</a></span></div>
								<p><a href="mailto:contato@ginecomastiatratamento.com.br" title="Envie um e-mail para nós" class="email-footer"><span itemprop="email">contato@ginecomastiatratamento.com.br</span></a></p>
							</div>
						</div>
					</div>
					<div class="row">
						<p>
							<div class="text-center footer-info">Médico Responsável: Dr. Wendell Fernando Uguetto <br> CRM-SP: 112.145</div>
						</p>
						<p>
							<div class="text-center footer-info">
								<a href="https://www.3xceler.com.br/criacao-de-sites" rel="noopener" title="Criação de sites" target="_blank">Criação de Sites </a> <span style="color: #FFF">Agência
								3xceler</span>
							</div>
						</p>
					</div>
				</div>
			</footer>
			<div class="cta-mobile">
				<div class="right">
					<i class="fa fa-phone"></i>
					<p><a href="tel:1147501133" title="Ligue para nós" class="fone-mobile">ligar agora</a></p>
				</div>
				<div class="left">
					<i class="fa fa-whatsapp"></i>
					<p><a href="wpp" target="_BLANK">Whatsapp</a></p>
				</div>
			</div>
			<div id="lightbox" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<div class="modal-content">
						<div class="modal-body">
							<form role="form" class="col-md-9 go-right" id="form-ligamos">
								<!-- NESP -->
								<input type="hidden" name="url" value="<?= " https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>">
								<!-- NESP -->
								<input type="hidden" name="tipo" value="ligamos">
								<h2>Nós te ligamos</h2>
								<p>Preencha o formulário abaixo, retornaremos o mais breve possível.</p>
								<div class="form-group">
									<input name="nome" type="text" class="form-control" placeholder="Nome" required>
								</div>
								<div class="form-group">
									<input name="tel" type="tel" class="form-control" placeholder="Telefone" required>
								</div>
								<div class="form-group">
									<input name="email" type="email" class="form-control" placeholder="E-mail" required>
								</div>
								<div class="form-group">
									<button type="submit">Enviar</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- <script src="dist/app.js "></script> localhost -->
			 <script src="/dist/app.js "></script>
			<?php 
			if( $bodyClass=='unidade' ) : 
				echo '<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBz-7mxMmUKgQ87XkuhOMGqZS1rwdf_GD0"></script>';
				?>
				<?php
			endif; 
			?>
			<div class="background-menu"></div>
			<a href="wpp" class="whatsapp-cta" title="Clique para falar conosco no Whatsapp!"
			target="_BLANK">
			<i class="fa fa-whatsapp"></i>
			<p>Fale conosco no <b>WhatsApp</b>!</p>
		</a>
		<div class="overlay">
			<div class="informative">
				<button class="close-informative" aria-label="Fechar Informativo">&times;</button>
				<img src="css/assets/05.png" alt="Dr. Wendell vestindo jaleco branco e sorrindo" title="Dr. Wendell" class="wendell-persona">
				<div class="text-box">
					<h1 class="section-title small blue has-border light-teal-border smaller-margin">
						IMPORTÂNCIA DA
						<span class="informative-title shaded-text">INFORMAÇÃO E DO DIAGNÓSTICO</span>
					</h1>
					<p class="generic-text gray regular smaller-paragraph">
						Muitos pacientes chegam até a clínica com certo receio ou hesitação sobre o tratamento da Ginecomastia.
					</p>
					<p class="generic-text gray regular smaller-paragraph">
						Eu costumo dizer que isso é resultado da falta de informação, principalmente pelo fato das pessoas buscarem
						informações em fontes que não são o médico cirurgião especialista.
						O primeiro e mais importante passo para tratar essa condição é a consulta e o diagnóstico. Afinal, caso a patologia
						seja identificada de maneira prematura, será possível seguir com um tratamento medicamentoso, evitando assim, uma
						intervenção cirúrgica.
					</p>
					<p class="generic-text gray regular smaller-paragraph">
						No portal de nossa clínica, você vai encontrar informações em uma linguagem simples, para que possa compreender um
						pouco melhor o que é a Ginecomastia. Porém, quero reforçar que as informações contidas em meu site são meramente
						informativas e, de maneira alguma, substituem a consulta médica ou devem ser utilizadas para um autodiagnostico.
					</p>
					<p class="generic-text gray regular smaller-paragraph">
						Por isso, reitero que a consulta médica com um especialista é essencial para um diagnóstico correto, proporcionando
						a você o melhor tratamento para a cura da Ginecomastia.
					</p>
					<p class="generic-text blue slightly-bigger">
						<b>
							Para mais informações ou agendamento de consulta deixe seu nome e telefone que nossos atendentes entrarão em
							contato:
						</b>
					</p>
					<div class="informative-form-wrapper dib-father">
						<form class="informative-form">
							<label class="field-wrapper dib">
								<input type="hidden" name="tipo" value="informativo">
								<input type="text" class="field-leaf" name="nome" placeholder="Nome*" required>
							</label>
							<label class="field-wrapper dib">
								<input type="text" class="field-leaf" name="telefone" placeholder="Telefone*" required>
							</label>
							<label class="button-wrapper dib">
								<button class="submit-informative">
									ENVIAR
								</button>
							</label>
						</form>
						<p class="generic-text gray slightly-bigger informative-call">
							<span class="generic-text smaller">
								Ou ligue:
							</span>
							&nbsp;
							<a href="tel:1147501133" class="fone-informative" title="Ligue para nós!">
								<i class="fa fa-phone t-teal"></i>
								11 4750.1133
							</a>
							&nbsp; | &nbsp;
							<a href="wpp" class="whats-informative" target="_BLANK" rel="nofollow noreferer" title="Fale conosco no Whatsapp!">
								<i class="fa fa-whatsapp t-teal"></i>
								11 98211.0450
							</a>
						</p>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript" src="https://phonetrack-static.s3.sa-east-1.amazonaws.com/aebf7782a3d445f43cf30ee2c0d84dee.js"
		id="script-pht-phone" data-cookiedays="5"></script>
				<!-- Tag Telefone -->
		<script type="text/javascript">
			(function (a, e, c, f, g, b, d) {
				var h = {
					ak: "882336618",
					cl: "o5-QCKmhr2YQ6sbdpAM"
				};
				a[c] = a[c] || function () {
					(a[c].q = a[c].q || []).push(arguments)
				};
				a[f] || (a[f] = h.ak);
				b = e.createElement(g);
				b.async = 1;
				b.src = "//www.gstatic.com/wcm/loader.js";
				d = e.getElementsByTagName(g)[0];
				d.parentNode.insertBefore(b, d);
				a._googWcmGet = function (b, d, e) {
					a[c](2, b, h, d, null, new Date, e)
				}
			})(window, document, "_googWcmImpl", "_googWcmAk", "script");
			_googWcmGet('tel-1', '11-2893-3348');
		</script>
		<script>
			window.BulldeskSettings = {
				token: 'c8004164ace7eeb8e2941c8ca33b5d00'
			};
			! function (a, b, c) {
				if ('object' != typeof c) {
					var d = function () {
						d.c(arguments)
					};
					d.q = [], d.c = function (a) {
						d.q.push(a)
					}, a.Bulldesk = d;
					var e = b.createElement('script');
					e.type = 'text/javascript', e.async = !0, e.id = 'bulldesk-analytics', e.src =
					'https://static.bulldesk.com.br/bulldesk.js';
					var f = b.getElementsByTagName('script')[0];
					f.parentNode.insertBefore(e, f)
				}
			}(window, document, window.Bulldesk);
		</script>
	</body>
</html>