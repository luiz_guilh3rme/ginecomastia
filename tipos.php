<?php 
$bodyClass = 'interna';
$title = 'Ginecomastia | Ginecomastia Tratamento';
$description = '';
$cannonical = '';
$message = 'Entre em contato conosco';
$type = 'contato';
include 'header.php';

?>
<section class="principal">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
				<h1 class="text-uppercase"><span class="help-block">tipos da  </span><span class="subtitle">ginecomastia</span></h1>
				<p>
					Você sabia que 80% dos homens com ginecomastia tem problemas em tirar a camiseta em púb Você sabia que 80% dos homens com ginecomastia tem problemas em tirar a camiseta Você sabia que 80% dos homens com ginecomastia tem problemas em tirar a camiseta em púb. 
				</p>
				<p>
					Você sabia que 80% dos homens com ginecomastia tem problemas. Você sabia que 80% dos homens com ginecomastia tem problemas em tirar a camiseta em púb Você sabia que 80% dos homens com ginecomastia tem problemas em tirar a camiseta Você sabia que. 
				</p>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-1 col-lg-6 col-lg-offset-1 formulario"> 
				<?php include 'form-topo.php';?>
			</div>
		</div>
	</div>
</section>
<section class="mais">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
				<h2 class="section-title">Classificação da Ginecomastia</h2>
				<p>
					Diversas classificações foram propostas para a diferenciar a Ginecomastia. Destas, utilizamos 2 classificações, uma histológica e outra clínica.
				</p>
				<p>
					Em 1972, Bannayan e Hajdu classificaram a ginecomastia em três tipos histológicos:
				</p>
				<p>
					<strong>Tipo florido: </strong>
					é a forma ativa da ginecomastia, há intensa atividade de proliferação dos tecidos mamários. Tem curta duração, geralmente inferior a quatro meses.
				</p>
				<p>
					<strong>Tipo fibroso: </strong>
					é a forma inativa da ginecomastia, as alterações do tecido mamário já se estabilizaram, com presença de fibrose. Mais comum após um ano de duração.
				</p>
				<p>
					<strong>Tipo intermediário: </strong>
					é uma forma mista de ginecomastia, com áreas floridas e áreas fibróticas. Encontrado entre 5 e 12 meses de duração. 
				</p>
				<p>
					Essa classificação é útil para entendermos que apenas ginecomastias em atividade, ou seja, do tipo florido ou intermediário terão resposta ao tratamento medicamentoso. Quando o tempo de evolução da ginecomastia é maior que 1 ano, teremos grande parte do tecido mamário do tipo fibroso, que não responde aos medicamentos, portanto o tratamento é de indicação cirúrgica.
				</p>
				<p>
					Para a classificação clínica, a grande maioria da literatura médica mundial utiliza a classificação proposta por Simon em 1973, devido a sua simplicidade de reprodução e por sua correlação clínico-cirúrgica. Tipicamente, considera-se uma classificação de quatro graus, que vai do mais leve, com uma hipertrofia mínima e sem sobra de pele ao mais grave, com um volume superior a 500 gramas e com ptose mamária acentuada.
				</p>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-1 col-lg-6 col-md-offset-1">
				<h2 class="section-title">Significado de Cada Grau</h2>
				<h4 class="article-subtitle">Grau 1</h4>
				<p>
					Pacientes com grau I de Simon geralmente apresentam uma massa de glândula mamária abaixo da aréola de não mais que 250g, a qual fica saltada, principalmente quando o mamilo se contrai. Este pequeno aumento do volume mamário já é capaz de incomodar no uso de camisas justas, que ficam marcadas na região do mamilo.
				</p>
				<h4 class="article-subtitle">Grau 2 (A e B) </h4>
				<p>
					No grau 2 a quantidade de tecido mamário é maior, varia entre 250 a 500 gramas e não está restrita apenas à região infra-areolar, acometendo boa parte do tórax. O que diferencia o grau 2a do 2b é o excesso de pele. Nestes casos, o incômodo com a aparência das mamas é maior que no tipo I, pois são facilmente percebidas mesmo com roupa.
				</p>
				<h4 class="article-subtitle">Grau 3 </h4>
				<p>
					No grau III as mamas são grandes (maiores que 500 gramas) e ptosadas (caídas). Nestes casos, a ginecomastia é muito aparente, difícil de disfarçar e pode ocasionar impactos psicológicos desastrosos no homem. 
				</p>
				<h2 class="section-title">Classificação da Ginecomastia (Simon, 1973)</h2>
				<p>
					<strong>Grau 1 -</strong> pequeno aumento da mama, sem sobra de pele
				</p>
				<p>
					<strong>Grau 2A </strong>- aumento moderado da mama, sem sobra de pele
				</p>
				<p>
					<strong>Grau 2B </strong>- aumento moderado da mama, com excesso de pele
				</p>
				<p>
					<strong>Grau 3 -</strong> grande aumento da mama, com excesso de pele
				</p>
			</div>
		</div>
		<?php include 'conheca-mais.php'; ?>
	</div>
</section>


<?php 
include 'ask.php';
include 'footer.php';
?>