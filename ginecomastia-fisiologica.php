<?php 
$bodyClass = 'interna';
$title = 'Ginecomastia Fisiológica | Ginecomastia Tratamento';
$description = 'Ginecomastia Fisiológica - Ocorre por alterações do próprio organismo e que são normais para cada fase da vida. Saiba quais são e como ocorrem!';
$cannonical = 'https://www.ginecomastiatratamento.com.br/ginecomastia-fisiologica/';
$message = 'Entre em contato conosco';
$type = 'contato';
include 'header.php';

?>
<div itemscope itemtype="http://schema.org/WebPage">
	<div class="container">
		<div class="row">
		<div class="breadcrumb">
				<ul itemscope itemtype="http://schema.org/BreadcrumbList">
					<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
						<a itemprop="item" href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/"><i class="fa fa-home" ></i>
						<span itemprop="name">home</span>
						</a>
						<meta itemprop="position" content="1" />
					</li>
					<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
						<a itemprop="item" href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/causas-da-ginecomastia/">
						<span itemprop="name">Causas da Ginecomastia</span>
						</a>
						<meta itemprop="position" content="2" />
					</li>
					<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
						<span itemprop="name" class="active">Ginecomastia Fisiológica</span>
						<meta itemprop="position" content="3" />
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<section class="main-content">
	<div class="container">
		<div class="row row-border">
			<div class="content">
				<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">
					<h1 class="text-uppercase section-title text-blue">Ginecomastia Fisiológica</h1>
					<p>
						A ginecomastia fisiológica acontece principalmente em 3 períodos principais da vida do homem: logo após o nascimento, na adolescência e na andropausa. A causa é chamada de fisiológica porque não há um fator causal responsável pela ginecomastia, como por exemplo uma doença ou medicamento, mas sim alterações do próprio organismo e que são normais para cada fase da vida.
					</p>
				</div>
			</div>
			<div class="content">
				<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6"> 
					<div class="formulario">
						<?php include 'form-topo.php';?>
					</div>
					
				</div>	
			</div>
			
		</div>
		<div class="padding"></div>
		<div class="row">
			<div class="col-lg-9 row-border">
				<div class="col-sm-12 col-xs-12 col-lg-5 col-md-5">
					<div class="row">
						<div class="content">
							<p>
								No recém nascido o aumento das mamas é muito comum e se dá pela grande quantidade de hormônios femininos (estrógenos) que passaram da mãe para o bebê no final da gestação. Estes homônios são responsáveis pelo estímulo do crescimento do tecido mamário no período neonatal.
							</p>
							<p>
								A adolescência é uma fase de muitas mudanças tanto psicológicas quanto físicas que culminam na transformação do menino para o jovem. Nesta fase os testículos ainda são imaturos e há extrema oscilação nos níveis hormonais do organismo, os quais podem provocar a ginecomastia.
							</p>
							
						</div>
						
					</div>
				</div>
				<div class="col-sm-12 col-xs-12 col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1">
					<div class="row">
						<div class="content">
							<p>
								A andropausa também é uma fase de muitas alterações no corpo do homem, este passa a perder massa muscular, aumenta a quantidade de gordura corporal e os testículos sintetizam menos hormônios masculinos. A queda nos níveis de testosterona e um aumento nos níveis da estrógenos é a principal causa da ginecomastia após os 55 anos de idade.
							</p>
						</div>
						
					</div>		
				</div>
			</div>
			<div class="col-sm-12 col-xs-12 col-md-3 col-lg-3 submenu">
				<div class="text-uppercase indice-title"><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/causas-da-ginecomastia/">Causas da Ginecomastia:</a></div>
				<ul>
					<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-idiopatica/">Ginecomastia Idiopática</a></li>
					<li>
						<a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-fisiologica/" class="active">
							Ginecomastia Fisiológica
						</a>
						<ul class="sublist">
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-puberal/">Ginecomastia Puberal</a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-senil/">Ginecomastia Senil</a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-neonatal/">Ginecomastia Neonatal</a></li>
						</ul>
					</li>
					<li>
						<a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-patologica/">
							Ginecomastia Patológica
						</a>
						<ul class="sublist">
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-patologica-nao-tumorais/">Ginecomastia Não-Tumoral</a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-patologica-tumoral/">Ginecomastia Tumoral</a></li>
							<li><a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-causas-medicamentosas/">Ginecomastia Medicamentosa</a></li>
						</ul>
					</li>

				</ul>
			</div>
		</div>
	</div>

</section>

<section class="mais">
	<div class="container">
		<div class="row">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 articles">
					<h2 class="section-title article-title">Conheça mais sobre Ginecomastia</h2>
					<div class="row">
						<?php
						include 'includes/partials/o-que-e.php';
						include 'includes/partials/graus.php';
						include 'includes/partials/causas.php';
						include 'includes/partials/tratamento.php';
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<?php 
include 'ask.php';
include 'footer.php';
?>