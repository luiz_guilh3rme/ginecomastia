<?php 
$bodyClass = 'interna success';
$title = 'Obrigado pelo contato | Ginecomastia Tratamento';
$description = '';
$cannonical = '';
include 'header.php';

?>
<section class="main-content">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<div class="img-success"><img src="<?='http://'.$_SERVER["HTTP_HOST"] ?>/css/assets/success.svg" class="img-resposive" alt=""></div>
				<div class="title text-uppercase">Obrigado!</div>
				<div class="text-error text-center text-uppercase">Sua Mensagem foi enviada com sucesso! </div>
				<a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/" class="btn saibamais text-uppercase">Voltar para home</a>
			</div>
		</div>
	</div>
</section>

<!--pixel facebook-->
<script>
fbq('track', 'Lead', {
value: 1.00,
currency: 'USD'
});
</script>

<!-- Google Code for Leads Conversion Page -->

<script type="text/javascript">

/* <![CDATA[ */

var google_conversion_id = 882336618;

var google_conversion_language = "en";

var google_conversion_format = "3";

var google_conversion_color = "ffffff";

var google_conversion_label = "5e-ECKCbr2YQ6sbdpAM";

var google_remarketing_only = false;

/* ]]> */

</script>
<div style="position: absolute;bottom: 0;opacity: 0; z-index: -1;">
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">

</script>

<noscript>

<div style="display:inline;">

<img height="1" width="1" style="border-style:none; " alt="" src="//www.googleadservices.com/pagead/conversion/882336618/?label=5e-ECKCbr2YQ6sbdpAM&amp;guid=ON&amp;script=0"/>
</div>


</div>

</noscript>
<?php include 'footer.php'; ?>