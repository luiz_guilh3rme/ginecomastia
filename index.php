<?php



$bodyClass = 'home';

$title = 'Ginecomastia tem Tratamento! | Dr. Wendell Uguetto ';

$description = 'Ginecomastia Tratamento:ginecomastia é uma situação comum que corresponde ao crescimento de tecido glandular das mamas do homem. Faça uma consulta com o Dr. Wendell Uguetto!';

$cannonical = 'https://www.ginecomastiatratamento.com.br/';

include 'header.php';

?>

<section class="slider principal">

	<div id="background-carousel" class="background-carousel carousel slide carousel-fade" data-ride="carousel">

		<ol class="carousel-indicators">

			<li data-target="#background-carousel" data-slide-to="0" class="active"></li>

			<li data-target="#background-carousel" data-slide-to="1"></li>

			<!-- <li data-target="#background-carousel" data-slide-to="2"></li> -->

		</ol>

		<div class="carousel-inner" role="listbox">

			<div class="item destaque-1 active">

				<div class="col-md-12 text-center">

					<h1 class="text-uppercase title"><span>Ginecomastia tem </span><span class="subtitle">tratamento</span></h1>



					<div class="col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3">

						<p>

							A Ginecomastia - Tratamento tem como sua principal fórmula o acompanhamento criterioso do problema.

							Conheça os tratamentos para Ginecomastia!

						</p>

					</div>

					<div class="col-md-2 col-md-offset-5 col-lg-2 col-lg-offset-5">

						<a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/tratamento-para-ginecomastia/" class="btn saibamais" title="Tratamentos para Ginecomastia">SAIBA MAIS</a>

					</div>

				</div>

			</div>

			<div class="item destaque-2">

				<div class="col-md-12 text-center">

					<h2 class="text-uppercase title"><span>Tudo sobre</span><span class="subtitle"> Ginecomastia</span></h2>

					<div class="col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3">

						<p>

							Aqui você encontra todas as informações e esclarece suas dúvidas sobre a Ginecomastia - Tratamento, o aumento anormal das mamas masculinas

						</p>

					</div>

					<div class="col-md-2 col-md-offset-5 col-lg-2 col-lg-offset-5">

						<a href="https://www.ginecomastiatratamento.com.br/blog/tudo-sobre-ginecomastia/" class="btn saibamais" title="Saiba tudo sobre Ginecomastia">SAIBA MAIS</a>

					</div>

				</div>

			</div>

			<!-- <div class="item destaque-3">

				<div class="col-md-12 text-center">

					<h2 class="text-uppercase title"><span>Bons </span><span class="subtitle">Doutores</span><span> sabem resolver</span></h2>

					<div class="col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3">

						<p>

							Para isso ocorrer, procurar especialistas de grande gabarito, como o Dr. Wendell Uguetto é a saída.

						</p>

					</div>

					<div class="col-md-2 col-md-offset-5 col-lg-2 col-lg-offset-5">

						<a href="#" class="btn saibamais">SAIBA MAIS</a>

					</div>

				</div>

			</div>-->

		</div>

	</div>

</section>

<section class="especialista">

	<div class="container">

		<div class="row">

			<h2 class="text-center section-title">ESPECIALISTA EM GINECOMASTIA TRATAMENTO</h2>

			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

				<article>

					<img src="css/assets/icone-certificado.png" alt="Ícone Certificado">

					<h3 class="article-title">Credibilidade é o seu <span>sinônimo</span></h3>

					<p>

						Especialista em Ginecomastia - Tratamento, o Dr. Wendell Uguetto é um dos mais respeitados cirurgiões plásticos do ramo, ganhador de vários prêmios, como o Quality de melhor cirurgião de 2011.

					</p>

				</article>

				<article>

					<img src="css/assets/icone-estetoscopio.png" alt="Ícone Estetoscopio" class="icon2">

					<h3 class="article-title">Prazer em <span>fazer o bem</span></h3>

					<p>

						O especialista em ginecomastia tratamento, Dr. Wendell Uguetto, criou uma legião de clientes satisfeitos em seu tratamento, principalmente pela confiança que inspira em seus pacientes. Um profissional aplicado, que conquista, ano a ano, um respeito ainda maior no ramo clínico.

					</p>

				</article>

			</div>

			<div class="doctor"></div>

		</div>

	</div>

</section>

<section class="ask">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 formulario">
			<?php include('form-pergunte.php'); ?>
		</div>
	</div>
</div>
</section>

<section class="ginecomastia">

	<div class="container">

		<div class="row">

			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

				<article class="text-center">

					<h2 class="section-title first-title">O que é Ginecomastia?</h2>

					<p>A Ginecomastia é o desenvolvimento anormal de mamas em homens. (Do grego gyne = feminino, mastia = mamas, ou seja mamas femininas). Este aumento pode ser de componente glandular (glândula mamária), adiposo (gordura) ou misto.</p>

					<p>Aparece inicialmente com um nódulo endurecido abaixo da aréola de pequeno tamanho (de 0,5 cm a 2 cm), com crescimento gradual. Nesta fase, a dor costuma incomodar bastante, causando muito desconforto ao simples toque. Passados cerca de 4 semanas, a sensação dolorosa tende a desaparecer, mas a mama continua a crescer ao longo de alguns meses.</p>

					<a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/o-que-e-ginecomastia/" class="btn saibamais">SAIBA MAIS</a>

				</article>

			</div>

			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

				<article class="text-center">

					<h2 class="section-title"><span>Dr.</span> Wendell Uguetto</h2>

					<p>

						Wendell é nascido em São Paulo. Sua família morou em Belém, Pará e Capivari, interior de São Paulo. Na cidade paulista, Uguetto estudou no Anglo Capivari, onde terminou o colegial e conseguiu passar em 6 faculdades de medicina (sendo 5 estaduais/federais e apenas uma particular).

					</p>

					<p>

						Diante de tantas opções, Uguetto optou pela USP Pinheiros por ser a melhor faculdade de Medicina do país, realizando teu sonho. Em questão de anos, o menino sonhador se transformaria em um dos médicos mais respeitados no ramo de cirurgia plástica no país.

					</p>

					<a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/dr-wendell-uguetto/" class="btn saibamais">SAIBA MAIS</a>

				</article>

			</div>

		</div>

	</div>

</section>

<section class="fases">

	<div class="container">

		<div class="row text-center">

			<h2 class="section-title text-center">NÍVEIS DO PROBLEMA <span></span></h2>

			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 fase-container">

				<div class="fase1"></div>

				<h3 class="section-subtitle">Ginecomastia Grau 1</h3>

				<p>

					Pacientes com ginecomastia grau I de Simon apresentam uma massa de glândula mamária abaixo da aréola de não mais que 250g.

				</p>

				<a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/graus-da-ginecomastia/">Saiba mais</a>

			</div>

			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 fase-container">

				<div class="fase2"></div>

				<h3 class="section-subtitle">Ginecomastia Grau 2</h3>

				<p>

					Na ginecomastia grau 2 a quantidade de tecido mamário é maior, varia entre 250 a 500 gramas e não está restrita apenas à região infra-areolar.

				</p>

				<a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/graus-da-ginecomastia/">Saiba mais</a>

			</div>

			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 fase-container">

				<div class="fase3"></div>

				<h3 class="section-subtitle">Ginecomastia Grau 3</h3>

				<p>

					Na ginecomastia grau 3 as mamas são grandes (maiores que 500 gramas) e ptosadas (caídas). Nestes casos, o problema é muito aparente.

				</p>

				<a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/graus-da-ginecomastia/">Saiba mais</a>

			</div>

			<div class="barra"></div>

		</div>

	</div>

</div>

</section>

<section class="tratamento">

	<div class="container">

		<div class="row">

			<div class="col-lg-12"><h2 class="section-title text-center text-uppercase">Tratamentos Possíveis</h2></div>

			<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 text-right">

				<h3 class="section-subtitle">Medicamentoso</h3>

				<p>

					O uso de medicamentos é indicado nos casos recentes e doloridos, com constrangimento ou desconforto emocional. Quando estes sintomas não estiverem presentes, o tratamento pode ser expectante, uma vez que boa parte dos casos terá resolução completa e de forma espontânea em até 1 ano do início dos sintomas.

				</p>

				<a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/ginecomastia-tratamento-medicamentoso/">Saiba mais</a>

			</div>

			<div class="col-xs-12 col-sm-12 col-md-5 col-md-offset-2 col-lg-5 col-lg-offset-2 text-left">

				<h3 class="section-subtitle">Cirúrgico</h3>

				<p>

					A correção cirúrgica é indicada nos pacientes que não tiveram melhora completa com o tratamento via medicamentoso ou em casos de doença avançada com mais de 12 a 18 meses de progressão. Existem várias técnicas de cirurgias que podem ser realizadas, trazendo resultados bastante satisfatórios.

				</p>

				<a href="<?='http://'.$_SERVER["HTTP_HOST"] ?>/cirurgia-ginecomastia/">Saiba mais</a>

			</div>

		</div>

	</div>

</section>



<?php

include 'footer.php';

?>

